package eu.europeanainside.validation.utils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

public class TransformerTest {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(TransformerTest.class);
  
  private static final String TEST_TRANSFORM = "transform/lidotest.xsl";
  private static final String TEST_RECORD = "transform/lido-sample.xml";

  @Test
  public void testTransform() throws Exception {
    InputStream transform = new ClassPathResource(TEST_TRANSFORM).getInputStream();
    InputStream record = new ClassPathResource(TEST_RECORD).getInputStream();
    String result = new String(Transformer.transform(transform, record).toByteArray());
    LOGGER.info(result);
  }

}
