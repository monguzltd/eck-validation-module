package eu.europeanainside.validation;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class ValidatorPluginManagerIT {

  private RestTemplate restTemplate;
  private static final String TEST_CONTEXT = "classpath*:test-context.xml";
  private final String springXml =
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
          + "<beans xmlns=\"http://www.springframework.org/schema/beans\" "
          + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
          + "xmlns:xsd-validator=\"http://www.europeanainside.eu/schema/xsd-validator\" "
          + "  xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd "
          + " http://www.europeanainside.eu/schema/xsd-validator http://www.europeanainside.eu/schema/xsd-validator/xsd-validator.xsd\"> "
          + "<bean class=\"eu.europeanainside.validation.ValidatorPluginManager\">"
          + "<property name=\"plugins\">"
          + "<list>"
          + "<ref bean=\"schemaPlugin\" />"
          + "</list>"
          + "</property>"
          + "</bean>"
          + "<xsd-validator:define name=\"schemaPlugin\" xsdPath=\"http://www.lido-schema.org/schema/v1.0/lido-v1.0.xsd\" />"
          + "</beans>";
  private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorPluginManager.class);

  @Before
  public void setUp() {
    ClassPathXmlApplicationContext serverContext = new ClassPathXmlApplicationContext(TEST_CONTEXT);
    restTemplate = serverContext.getBean("restTemplate", RestTemplate.class);
  }

  // @Test
  public void test() throws URISyntaxException, IOException {
    Path file = Paths.get("/home/kszucs/Kepzomuveszeti_alkotas_referencia_LIDO_gml-el.xml");
    Charset charset = Charset.defaultCharset();
    LOGGER.info("charset: " + charset.displayName());
    List<String> content = Files.readAllLines(file, charset);
    URI uri2 = new URI("http://localhost:29999/eck-validation/profiles/profile5/validate");
    StringBuilder record = new StringBuilder();
    for (String line : content) {
      record.append(line.trim());
    }
    String recordString = record.toString();

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_XML);

    HttpEntity<String> contentEntity = new HttpEntity<String>(recordString, headers);

    String result = restTemplate.postForObject(uri2, contentEntity, String.class);
    LOGGER.info(result);
  }

  @Test
  public void testCreateProfile() throws URISyntaxException {
    URI uri = new URI("http://localhost:29999/eck-validation/profiles/profile2");

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.TEXT_PLAIN);

    HttpEntity<String> contentEntity = new HttpEntity<String>(springXml, headers);

    restTemplate.postForEntity(uri, contentEntity, Void.class);
  }
}
