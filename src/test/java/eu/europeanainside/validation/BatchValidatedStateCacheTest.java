package eu.europeanainside.validation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.europeanainside.validation.utils.ConstantUtils;

/**
 * Unit test class for BatchValidatedStateCache class.
 * @author mbathori
 *
 */
public class BatchValidatedStateCacheTest {

	private final String TEST_SET = "Test set";
	private final String TEST_SET_2 = "Test set 2";
	private final String TEST_PROVIDER = "Test provider";
	private final String TEST_PROVIDER_2 = "Test provider 2";
	private final String TEST_STATUS = "Test status";
	
	BatchValidatedStateCache batchValidatedStateCache;
	
	@Before
	public void setUp() {
		batchValidatedStateCache = new BatchValidatedStateCache();
		batchValidatedStateCache.putIdentifier(TEST_SET, TEST_PROVIDER);	
	}
	
	@Test
	public void putIdentifierTest() {
		batchValidatedStateCache.putIdentifier(TEST_SET_2, TEST_PROVIDER_2);	
		String state = batchValidatedStateCache.getValidateState(TEST_SET_2, TEST_PROVIDER_2);
		assertEquals("Put identifier test", ConstantUtils.VALIDATION_PROCESSING_STATUS, state);
	}
	
	@Test
	public void updateValidatedStatusTest() {
		batchValidatedStateCache.updateValidatedStatus(TEST_SET, TEST_STATUS, TEST_PROVIDER);
		String state = batchValidatedStateCache.getValidateState(TEST_SET, TEST_PROVIDER);
		assertEquals("Update validation status", TEST_STATUS, state);
	}
	
	@Test
	public void doesValidationSetExistTest() {
		boolean result = batchValidatedStateCache.doesValidationSetExist(TEST_SET, TEST_PROVIDER);
		boolean result2 = batchValidatedStateCache.doesValidationSetExist(TEST_SET_2, TEST_PROVIDER_2);

		assertEquals("Existing validation set", true, result);
		assertEquals("Nonexist validation set", false, result2);
	}
	
	@Test
	public void getOngoingValidationsTest() {
		List<String> ongoingValidations = batchValidatedStateCache.getOngoingValidations(TEST_PROVIDER);

		List<String> expectedResult = new ArrayList<String>();
		expectedResult.add(TEST_SET);
		
		assertEquals("Ongoing validations list", ongoingValidations, expectedResult);//TODO:assert hiba üzenetek
	}

}
