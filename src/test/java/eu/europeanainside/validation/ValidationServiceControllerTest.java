package eu.europeanainside.validation;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.europeanainside.validation.api.rest.ValidatorServicesController;
import eu.europeanainside.validation.exception.ProfileNotFoundException;
import eu.europeanainside.validation.exception.ValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/testApplicationContext.xml"})
public class ValidationServiceControllerTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ValidationServiceControllerTest.class);
  @Autowired
  ValidatorServicesController validatorServicesController;
  MockMultipartFile mockMultipartFile;
  String fileName = "testMultipart";
  byte[] profile = String.valueOf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    + "<beans xmlns=\"http://www.springframework.org/schema/beans\" "
    + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
    + "xmlns:xsd-validator=\"http://www.europeanainside.eu/schema/xsd-validator\" "
    + "xmlns:uri-validator=\"http://www.europeanainside.eu/schema/uri-validator\" "
    + "xmlns:link-validator=\"http://www.europeanainside.eu/schema/link-validator\"  "
    + "xsi:schemaLocation=\"http://www.springframework.org/schema/beans "
    + "http://www.springframework.org/schema/beans/spring-beans.xsd  "
    + "http://www.europeanainside.eu/schema/xsd-validator "
    + "http://www.europeanainside.eu/schema/xsd-validator/xsd-validator.xsd	"
    + "http://www.europeanainside.eu/schema/uri-validator "
    + "http://www.europeanainside.eu/schema/uri-validator/uri-validator.xsd	"
    + "http://www.europeanainside.eu/schema/link-validator "
    + "http://www.europeanainside.eu/schema/link-validator/link-validator.xsd\">    "
    + "<bean class=\"eu.europeanainside.validation.ValidatorPluginManager\">    "
    + "<property name=\"xmlRecordStartTag\" value=\"lido:lido\"/> "
    + "<property name=\"xmlRecordIdTag\" value=\"lido:lidoRecID\"/> "
    + "<property name=\"plugins\">      "
    + "<list>        "
    + "<ref bean=\"schemaPlugin\" />		"
    + "<ref bean=\"uriPlugin\" />		"
    + "<ref bean=\"linkPlugin\" />      "
    + "</list>    </property>  </bean>    "
    + "<xsd-validator:define name=\"schemaPlugin\" xsdPath=\"http://www.lido-schema.org/schema/v1.0/lido-v1.0.xsd\" />	"
    + "<uri-validator:define name=\"uriPlugin\" "
    + "tagName=\"lido:conceptID\" "
    + "attributeName=\"lido:type\" />	"
    + "<link-validator:define name=\"linkPlugin\" />"
    + "</beans>").getBytes();
  byte[] record = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><lido:lidoWrap xmlns:marc=\"http://www.loc.gov/MARC21/slim\"	xmlns:lido=\"http://www.lido-schema.org\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"	xmlns:xalan=\"http://xml.apache.org/xalan\" xmlns:gml=\"http://www.opengis.net/gml\">	<lido:lido>		<lido:lidoRecID lido:encodinganalog=\"001\"			lido:source=\"Magyar Nemzeti Múzeum / Hungarian National Museum\" >asd</lido:lidoRecID>		<lido:descriptiveMetadata xml:lang=\"hun\">			<lido:objectClassificationWrap>				<lido:objectWorkTypeWrap>					<lido:objectWorkType>						<lido:term />					</lido:objectWorkType>				</lido:objectWorkTypeWrap>				<lido:classificationWrap>					<lido:classification lido:type=\"europeana:type\">						<lido:term lido:addedSearchTerm=\"no\">IMAGE</lido:term>					</lido:classification>				</lido:classificationWrap>			</lido:objectClassificationWrap>			<lido:objectIdentificationWrap>				<lido:titleWrap>					<lido:titleSet>						<lido:appellationValue lido:encodinganalog=\"c520 ##$b c245 ##$a\"							lido:label=\"Műtárgy címe / Title\" lido:pref=\"preferred\" xml:lang=\"hun\" />					</lido:titleSet>				</lido:titleWrap>			</lido:objectIdentificationWrap>		</lido:descriptiveMetadata>		<lido:administrativeMetadata>			<lido:recordWrap>				<lido:recordID lido:encodinganalog=\"c001\"					lido:label=\"Rekordazonosító / Record identifier\" lido:pref=\"preferred\"					lido:source=\"Magyar Nemzeti Múzeum / Hungarian National Museum\"					lido:type=\"local\" />				<lido:recordType>					<lido:term>item</lido:term>				</lido:recordType>				<lido:recordSource lido:type=\"europeana:dataProvider\">					<lido:legalBodyName>						<lido:appellationValue>HNM online public access catalogue</lido:appellationValue>					</lido:legalBodyName>				</lido:recordSource>			</lido:recordWrap>			<lido:resourceWrap>				<lido:resourceSet>					<lido:resourceRepresentation lido:type=\"image_thumb\">						<lido:linkResource lido:formatResource=\"\" />					</lido:resourceRepresentation>					<lido:resourceRepresentation lido:type=\"image_master\">						<lido:linkResource lido:formatResource=\"\" />					</lido:resourceRepresentation>					<lido:rightsResource>						<lido:rightsType>							<lido:term lido:pref=\"preferred\" />						</lido:rightsType>					</lido:rightsResource>				</lido:resourceSet>			</lido:resourceWrap>		</lido:administrativeMetadata>	</lido:lido></lido:lidoWrap>"
    .getBytes();

  @Test
  public void multipartSaveProfileTest() throws IOException,
    ValidationException {
    mockMultipartFile = new MockMultipartFile("fileData", fileName,
      "text/plain", profile);

    validatorServicesController.multipartSaveProfile(mockMultipartFile,
      "testProvider", "testProfile");
  }

  @Test
  public void multipartSingleValidateTest()
    throws ProfileNotFoundException, IOException, ValidationException {
    mockMultipartFile = new MockMultipartFile("fileData", fileName,
      "text/plain", record);

    validatorServicesController.multipartSingleValidateAsXml(
      mockMultipartFile, "testProvider", "testProfile");
  }
}
