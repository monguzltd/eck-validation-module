package eu.europeanainside.validation;

import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.validation.exception.ProfileNotFoundException;

public class ProfileManagerTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProfileManagerTest.class);

  /**
   * TODO minta plugin config XML specifikálása.
   */
  @Test
  public void test() {
    ProfileManager manager = new ProfileManager();

    try {
      manager.saveProfile("this is it!", "name", "provider");
    } catch (IOException e) {
      LOGGER.error("save");
    }

    try {
      manager.saveProfile("this is not it!", "name2.profile", "provider");
    } catch (IOException e) {
      LOGGER.error("save2");
    }

    try {
      manager.getProfile("name", "provider");
    } catch (ProfileNotFoundException | IOException e) {
      LOGGER.error("get");
    }
    try {
      manager.getProfile("name2.profile", "provider");
    } catch (ProfileNotFoundException | IOException e) {
      LOGGER.error("get2");
    }

    try {
      manager.listProfiles("provider");
    } catch (IOException e) {
      LOGGER.error("list2");
    }

    try {
      manager.deleteProfile("name", "provider");
    } catch (IOException e) {
      LOGGER.error("delete");
    }

    try {
      manager.deleteProfile("name2.profile", "provider");
    } catch (IOException e) {
      LOGGER.error("delete2");
    }

  }
}
