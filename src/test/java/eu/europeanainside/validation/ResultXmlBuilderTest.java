package eu.europeanainside.validation;

import static org.custommonkey.xmlunit.XMLAssert.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Unit test class for ResultXmlBuilder class.
 * @author mbathori
 *
 */
public class ResultXmlBuilderTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ResultXmlBuilderTest.class);
  private ResultXmlBuilder resultXmlBuilder;
  private final String lidoRecord = "<?xml version='1.0' encoding='UTF-8'?><lido:lidoWrap xmlns:marc='http://www.loc.gov/MARC21/slim' xmlns:lido='http://www.lido-schema.org' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xalan='http://xml.apache.org/xalan' xmlns:gml='http://www.opengis.net/gml'><lido:lido><lido:lidoRecID lido:encodinganalog='001' lido:source='Magyar Nemzeti Múzeum / Hungarian National Museum' >recordIdentifier</lido:lidoRecID><lido:descriptiveMetadata xml:lang='hun'><lido:objectClassificationWrap><lido:objectWorkTypeWrap><lido:objectWorkType><lido:term /></lido:objectWorkType></lido:objectWorkTypeWrap><lido:classificationWrap><lido:classification lido:type='europeana:type'><lido:term lido:addedSearchTerm='no'>IMAGE</lido:term></lido:classification></lido:classificationWrap></lido:objectClassificationWrap><lido:objectIdentificationWrap><lido:titleWrap><lido:titleSet><lido:appellationValue lido:encodinganalog='c520 ##$b c245 ##$a' lido:label='Műtárgy címe / Title' lido:pref='preferred' xml:lang='hun' /></lido:titleSet></lido:titleWrap></lido:objectIdentificationWrap></lido:descriptiveMetadata><lido:administrativeMetadata><lido:recordWrap><lido:recordID lido:encodinganalog='c001' lido:label='Rekordazonosító / Record identifier' lido:pref='preferred' lido:source='Magyar Nemzeti Múzeum / Hungarian National Museum' lido:type='local' /><lido:recordType><lido:term>item</lido:term></lido:recordType><lido:recordSource lido:type='europeana:dataProvider'><lido:legalBodyName><lido:appellationValue>HNM online public access catalogue</lido:appellationValue></lido:legalBodyName></lido:recordSource></lido:recordWrap><lido:resourceWrap><lido:resourceSet><lido:resourceRepresentation lido:type='image_thumb'><lido:linkResource lido:formatResource='' /></lido:resourceRepresentation><lido:resourceRepresentation lido:type='image_master'><lido:linkResource lido:formatResource='' /></lido:resourceRepresentation><lido:rightsResource><lido:rightsType><lido:term lido:pref='preferred' /></lido:rightsType></lido:rightsResource></lido:resourceSet></lido:resourceWrap></lido:administrativeMetadata></lido:lido></lido:lidoWrap>";
  private final String xsdErrorResult = "<validationresult><record id=\"recordIdentifier\" result=\"error\"><error plugin=\"xsdValidator\">Test xsd error</error></record></validationresult>";
  private final String uriErrorResult = "<validationresult><record id=\"recordIdentifier\" result=\"error\"><error plugin=\"uriValidator\">Test uri error</error></record></validationresult>";
  private final String linkErrorResult = "<validationresult><record id=\"recordIdentifier\" result=\"error\"><error plugin=\"linkValidator\">Test link error</error></record></validationresult>";
  private final String successfulResult = "<validationresult><record id=\"recordIdentifier\" result=\"successful\"/></validationresult>";

  @Before
  public void setUp() {
    try {
      XMLUnit.setIgnoreWhitespace(true);
      resultXmlBuilder = new ResultXmlBuilder("lido:lidoRecID");
    } catch (ResultXmlBuildingException e) {
      LOGGER.debug("Error occured in resultXmlBuilderTest setUp method.");
    }
  }

  @Test
  public void getRecordNameTest() {
    try {
      String recordName = resultXmlBuilder.getRecordName(lidoRecord);
      assertEquals("recordIdentifier", recordName);

    } catch (ResultXmlBuildingException e) {
      LOGGER.debug("Error occured in getRecordNameTest function.");
    }
  }

  @Test
  public void addXSDValidationErrorTest() throws Exception {
    Exception ex = new Exception("Test xsd error");
    List<Exception> exceptions = new ArrayList<Exception>();
    exceptions.add(ex);

    resultXmlBuilder.addXSDValidationError(lidoRecord, exceptions);
    String result = resultXmlBuilder.documentToString();
    assertXMLEqual("Xsd error", result, xsdErrorResult);
  }

  @Test
  public void addURIValidationErrorTest() throws Exception {
    resultXmlBuilder.addURIValidationError(lidoRecord, "Test uri error");
    String result = resultXmlBuilder.documentToString();

    assertXMLEqual("Uri error", result, uriErrorResult);

  }

  @Test
  public void addLinkValidationErrorTest() throws Exception {
    resultXmlBuilder.addLinkValidationError(lidoRecord, "Test link error");
    String result = resultXmlBuilder.documentToString();

    assertXMLEqual("Link error", result, linkErrorResult);
  }

  @Test
  public void addSuccessfulRecordTest() throws Exception {
    resultXmlBuilder.addNewRecord(lidoRecord);
    String result = resultXmlBuilder.documentToString();

    assertXMLEqual("Successful record", result, successfulResult);
  }
}
