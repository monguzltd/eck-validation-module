<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:resource-validator="http://www.europeanainside.eu/schema/resource-validator"
       xmlns:xsd-validator="http://www.europeanainside.eu/schema/xsd-validator"
       xmlns:uri-validator="http://www.europeanainside.eu/schema/uri-validator"
       xmlns:link-validator="http://www.europeanainside.eu/schema/link-validator"
       xmlns:linkvalue-validator="http://www.europeanainside.eu/schema/linkvalue-validator"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans.xsd
http://www.europeanainside.eu/schema/xsd-validator
http://www.europeanainside.eu/schema/xsd-validator/xsd-validator.xsd
http://www.europeanainside.eu/schema/uri-validator
http://www.europeanainside.eu/schema/uri-validator/uri-validator.xsd
http://www.europeanainside.eu/schema/link-validator
http://www.europeanainside.eu/schema/link-validator/link-validator.xsd
http://www.europeanainside.eu/schema/resource-validator
http://www.europeanainside.eu/schema/resource-validator/resource-validator.xsd
http://www.europeanainside.eu/schema/linkvalue-validator
http://www.europeanainside.eu/schema/linkvalue-validator/linkvalue-validator.xsd ">
  <bean class="eu.europeanainside.validation.ValidatorPluginManager">
    <property name="xmlRecordStartTag" value="lido:lido"/>
    <property name="xmlRecordIdTag" value="lido:lidoRecID"/>
    <property name="plugins">
      <list>
        <ref bean="schemaPlugin" />
        <ref bean="uriPlugin" />
        <ref bean="resourcePlugin" />
        <ref bean="linkValuePlugin" />
      </list>
    </property>
  </bean>
  <xsd-validator:define name="schemaPlugin"
                        xsdPath="http://www.lido-schema.org/schema/v1.0/lido-v1.0.xsd" />
  <uri-validator:define name="uriPlugin"
                        tagName="lido:conceptID"
                        attributeName="lido:type"/>
  <resource-validator:define name="resourcePlugin"
                             resourceXpath="/descendant-or-self::lido/administrativeMetadata/resourceWrap/resourceSet/resourceRepresentation/linkResource" />
  <linkvalue-validator:define name="linkValuePlugin"
                              linkXpath="//rightsResource/rightsType/conceptID[@type='URL']"
                              validUrls="http://creativecommons.org/publicdomain/mark/1.0/,
http://www.europeana.eu/rights/out-of-copyright-non-commercial/,
http://www.europeana.eu/portal/rights/out-of-copyright-non-commercial.html,
http://creativecommons.org/publicdomain/zero/1.0/,
http://creativecommons.org/licenses/by/4.0/,
http://creativecommons.org/licenses/by-sa/4.0/,
http://creativecommons.org/licenses/by-nd/4.0/,
http://creativecommons.org/licenses/by-nc/4.0/,
http://creativecommons.org/licenses/by-nc-sa/4.0/,
http://creativecommons.org/licenses/by-nc-nd/4.0/,
http://www.europeana.eu/rights/rr-f/,
http://www.europeana.eu/rights/rr-p/,
http://www.europeana.eu/rights/orphan-work-eu/,
http://www.europeana.eu/rights/unknown/
http://www.europeana.eu/portal/rights/rr-f.html,
http://www.europeana.eu/portal/rights/rr-p.html,
http://www.europeana.eu/portal/rights/orphan-work-eu.html,
http://www.europeana.eu/portal/rights/unknown.html"
  />
</beans>