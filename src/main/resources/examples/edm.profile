<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd-validator="http://www.europeanainside.eu/schema/xsd-validator"
       xmlns:schematron-validator="http://www.europeanainside.eu/schema/schematron-validator"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans.xsd
http://www.europeanainside.eu/schema/xsd-validator
http://www.europeanainside.eu/schema/xsd-validator/xsd-validator.xsd
http://www.europeanainside.eu/schema/schematron-validator
http://www.europeanainside.eu/schema/schematron-validator/schematron-validator.xsd">
  <bean class="eu.europeanainside.validation.ValidatorPluginManager">
    <property name="xmlRecordStartTag" value="rdf:RDF"/>
    <property name="xmlRecordIdTag" value="dc:identifier"/>
    <property name="plugins">
      <list>
        <ref bean="schematronPlugin" />
        <ref bean="schemaPlugin" />
      </list>
    </property>
  </bean>
  <schematron-validator:define name="schematronPlugin"
                               rulesetLocation="schematron/schematron-rules.xsl" />
  <xsd-validator:define name="schemaPlugin"
                        xsdPath="file:edm/EDM-COMMON-MAIN.xsd" />
</beans>