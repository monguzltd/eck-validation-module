<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:saxon="http://saxon.sf.net/" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schold="http://www.ascc.net/xml/schematron" xmlns:iso="http://purl.oclc.org/dsdl/schematron" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:edm="http://www.europeana.eu/schemas/edm/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dct="http://purl.org/dc/terms/" xmlns:ore="http://www.openarchives.org/ore/terms/" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#"><!--Implementers: please note that overriding process-prolog or process-root is
    the preferred method for meta-stylesheets to use where possible. -->

   <xsl:param name="archiveDirParameter"/>
   <xsl:param name="archiveNameParameter"/>
   <xsl:param name="fileNameParameter"/>
   <xsl:param name="fileDirParameter"/>
   <xsl:variable name="document-uri">
      <xsl:value-of select="document-uri(/)"/>
   </xsl:variable>

<!--PHASES-->


<!--PROLOG-->

   <xsl:output method="xml" omit-xml-declaration="no" standalone="yes" indent="yes" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

<!--XSD TYPES FOR XSLT2-->


<!--KEYS AND FUNCTIONS-->


<!--DEFAULT RULES-->


<!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->

   <xsl:template match="*" mode="schematron-select-full-path">
      <xsl:apply-templates select="." mode="schematron-get-full-path"/>
   </xsl:template>

<!--MODE: SCHEMATRON-FULL-PATH-->
<!--This mode can be used to generate an ugly though full XPath for locators-->

   <xsl:template match="*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">
            <xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>*:</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>[namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="preceding" select="count(preceding-sibling::*[local-name()=local-name(current())                                   and namespace-uri() = namespace-uri(current())])"/>
      <xsl:text>[</xsl:text>
      <xsl:value-of select="1+ $preceding"/>
      <xsl:text>]</xsl:text>
   </xsl:template>
   <xsl:template match="@*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">@<xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>@*[local-name()='</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>' and namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

<!--MODE: SCHEMATRON-FULL-PATH-2-->
<!--This mode can be used to generate prefixed XPath for humans-->

   <xsl:template match="node() | @*" mode="schematron-get-full-path-2">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="preceding-sibling::*[name(.)=name(current())]">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template><!--MODE: SCHEMATRON-FULL-PATH-3-->
<!--This mode can be used to generate prefixed XPath for humans
	(Top-level element has index)-->

   <xsl:template match="node() | @*" mode="schematron-get-full-path-3">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="parent::*">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>

<!--MODE: GENERATE-ID-FROM-PATH -->

   <xsl:template match="/" mode="generate-id-from-path"/>
   <xsl:template match="text()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.text-', string(1+count(preceding-sibling::text())), '-')"/>
   </xsl:template>
   <xsl:template match="comment()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.comment-', string(1+count(preceding-sibling::comment())), '-')"/>
   </xsl:template>
   <xsl:template match="processing-instruction()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.processing-instruction-', string(1+count(preceding-sibling::processing-instruction())), '-')"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.@', name())"/>
   </xsl:template>
   <xsl:template match="*" mode="generate-id-from-path" priority="-0.5">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="concat('.',name(),'-',string(1+count(preceding-sibling::*[name()=name(current())])),'-')"/>
   </xsl:template>

<!--MODE: GENERATE-ID-2 -->

   <xsl:template match="/" mode="generate-id-2">U</xsl:template>
   <xsl:template match="*" mode="generate-id-2" priority="2">
      <xsl:text>U</xsl:text>
      <xsl:number level="multiple" count="*"/>
   </xsl:template>
   <xsl:template match="node()" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>n</xsl:text>
      <xsl:number count="node()"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="string-length(local-name(.))"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(name(),':','.')"/>
   </xsl:template><!--Strip characters-->
   <xsl:template match="text()" priority="-1"/>

<!--SCHEMA SETUP-->

   <xsl:template match="/">
      <svrl:schematron-output title="Schematron validation" schemaVersion="" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <xsl:comment>
            <xsl:value-of select="$archiveDirParameter"/>   
        <xsl:value-of select="$archiveNameParameter"/>  
        <xsl:value-of select="$fileNameParameter"/>  
        <xsl:value-of select="$fileDirParameter"/>
         </xsl:comment>
         <svrl:ns-prefix-in-attribute-values uri="http://www.europeana.eu/schemas/edm/" prefix="edm"/>
         <svrl:ns-prefix-in-attribute-values uri="http://purl.org/dc/elements/1.1/" prefix="dc"/>
         <svrl:ns-prefix-in-attribute-values uri="http://purl.org/dc/terms/" prefix="dct"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.openarchives.org/ore/terms/" prefix="ore"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2002/07/owl#" prefix="owl"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/1999/02/22-rdf-syntax-ns#" prefix="rdf"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2000/01/rdf-schema#" prefix="rdfs"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2004/02/skos/core#" prefix="skos"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M9"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M10"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M11"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M12"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M13"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M14"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M15"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M16"/>
      </svrl:schematron-output>
   </xsl:template>

<!--SCHEMATRON PATTERNS-->

   <svrl:text xmlns:svrl="http://purl.oclc.org/dsdl/svrl">Schematron validation</svrl:text>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="*" priority="1000" mode="M9">
      <svrl:fired-rule context="*" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(@rdf:resource = '')"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(@rdf:resource = '')" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
              Empty rdf:resource attribute is not allowed for <xsl:text/>
                  <xsl:value-of select="name(.)"/>
                  <xsl:text/> element.
            </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M9"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M9"/>
   <xsl:template match="@*|node()" priority="-2" mode="M9">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M9"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="*" priority="1000" mode="M10">
      <svrl:fired-rule context="*" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(@rdf:about = '')"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(@rdf:about = '')" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
              Empty rdf:about attribute is not allowed for <xsl:text/>
                  <xsl:value-of select="name(.)"/>
                  <xsl:text/> element.
            </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M10"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M10"/>
   <xsl:template match="@*|node()" priority="-2" mode="M10">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M10"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="*" priority="1000" mode="M11">
      <svrl:fired-rule context="*" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(@xml:lang = '')"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(@xml:lang = '')" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
              Empty xml:lang attribute is not allowed for <xsl:text/>
                  <xsl:value-of select="name(.)"/>
                  <xsl:text/> element.
            </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M11"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M11"/>
   <xsl:template match="@*|node()" priority="-2" mode="M11">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M11"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="*" priority="1000" mode="M12">
      <svrl:fired-rule context="*" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(@rdf:resource and text())"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(@rdf:resource and text())" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
              Element <xsl:text/>
                  <xsl:value-of select="name(.)"/>
                  <xsl:text/> should not have both rdf:resource attribute and text value populated.
            </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M12"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M12"/>
   <xsl:template match="@*|node()" priority="-2" mode="M12">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M12"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="edm:WebResource" priority="1000" mode="M13">
      <svrl:fired-rule context="edm:WebResource" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(dct:hasPart[text()])"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(dct:hasPart[text()])" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
              The element dcterms:isPartOf should not have a literal value in the edm:WebResource context
              with id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/>. Use an rdf:resource instead.
            </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M13"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M13"/>
   <xsl:template match="@*|node()" priority="-2" mode="M13">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M13"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="edm:ProvidedCHO" priority="1000" mode="M14">
      <svrl:fired-rule context="edm:ProvidedCHO" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="dc:subject or dc:type or (dc:coverage or dct:temporal) or dct:spatial"/>
         <xsl:otherwise>
            <svrl:failed-assert test="dc:subject or dc:type or (dc:coverage or dct:temporal) or dct:spatial" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/>
                - A ProvidedCHO must have a dc:subject or dc:type or dc:coverage or dct:temporal or dct:spatial.
              </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="dc:title or dc:description"/>
         <xsl:otherwise>
            <svrl:failed-assert test="dc:title or dc:description" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> - A ProvidedCHO must have a dc:title or dc:description.
              </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(edm:type='TEXT') or (edm:type='TEXT' and exists(dc:language))"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(edm:type='TEXT') or (edm:type='TEXT' and exists(dc:language))" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                id:	<xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> - dc:language is mandatory for TEXT objects.
              </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M14"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M14"/>
   <xsl:template match="@*|node()" priority="-2" mode="M14">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M14"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="ore:Aggregation" priority="1002" mode="M15">
      <svrl:fired-rule context="ore:Aggregation" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="edm:isShownAt or edm:isShownBy"/>
         <xsl:otherwise>
            <svrl:failed-assert test="edm:isShownAt or edm:isShownBy" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> An ore:Aggregation must have either
                edm:isShownAt or edm:isShownBy </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M15"/>
   </xsl:template>

	<!--RULE -->

   <xsl:template match="ore:Aggregation" priority="1001" mode="M15">
      <svrl:fired-rule context="ore:Aggregation" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="edm:dataProvider"/>
         <xsl:otherwise>
            <svrl:failed-assert test="edm:dataProvider" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> An ore:Aggregation must have at least one instance of
                edm:dataProvider </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M15"/>
   </xsl:template>

	<!--RULE -->

   <xsl:template match="ore:Aggregation" priority="1000" mode="M15">
      <svrl:fired-rule context="ore:Aggregation" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="edm:rights"/>
         <xsl:otherwise>
            <svrl:failed-assert test="edm:rights" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> An ore:Aggregation must have at least one instance of
                edm:rights </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M15"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M15"/>
   <xsl:template match="@*|node()" priority="-2" mode="M15">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M15"/>
   </xsl:template>

<!--PATTERN -->


	<!--RULE -->

   <xsl:template match="ore:Proxy" priority="1000" mode="M16">
      <svrl:fired-rule context="ore:Proxy" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="dc:subject or dc:type or (dc:coverage or dct:temporal) or dct:spatial"/>
         <xsl:otherwise>
            <svrl:failed-assert test="dc:subject or dc:type or (dc:coverage or dct:temporal) or dct:spatial" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text> id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> - A Proxy must have a
                dc:subject or dc:type or dc:coverage or dct:temporal or
                dct:spatial. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="dc:title or dc:description"/>
         <xsl:otherwise>
            <svrl:failed-assert test="dc:title or dc:description" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text> id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> - A Proxy must have a dc:title or
                dc:description. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(edm:type='TEXT') or (edm:type='TEXT' and exists(dc:language))"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(edm:type='TEXT') or (edm:type='TEXT' and exists(dc:language))" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text> id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/> - Within a Proxy
                context, dc:language is mandatory when dc:language has the value
                'TEXT'. </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="edm:type or (not(edm:type) and edm:europeanaProxy)"/>
         <xsl:otherwise>
            <svrl:failed-assert test="edm:type or (not(edm:type) and edm:europeanaProxy)" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text> id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/>
                edm:type should be present in an ore:Proxy context.
              </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		<!--ASSERT -->

      <xsl:choose>
         <xsl:when test="not(edm:type and edm:europeanaProxy)"/>
         <xsl:otherwise>
            <svrl:failed-assert test="not(edm:type and edm:europeanaProxy)" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text> id: <xsl:text/>
                  <xsl:value-of select="@rdf:about"/>
                  <xsl:text/>
                edm:type should not be present in an  Europeana Proxy context
                (when the edm:europeanaProxy value is present).
              </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M16"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M16"/>
   <xsl:template match="@*|node()" priority="-2" mode="M16">
      <xsl:apply-templates select="*|comment()|processing-instruction()" mode="M16"/>
   </xsl:template>
</xsl:stylesheet>