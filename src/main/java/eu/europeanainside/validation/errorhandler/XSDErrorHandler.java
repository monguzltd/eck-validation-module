package eu.europeanainside.validation.errorhandler;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * This class handles XSD validation exceptions.
 * @author mbathori
 *
 */
public class XSDErrorHandler implements ErrorHandler {
	
	private List<Exception> exceptions = new ArrayList<Exception>();
	
	@Override
	public void warning(SAXParseException exception) throws SAXException {
		exceptions.add(exception);
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		exceptions.add(exception);
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		exceptions.add(exception);
	}

	/**
	 * Gets the list of the validation exceptions.
	 * @return list of validation exceptions
	 */
	public List<Exception> getExceptions() {
		return exceptions;
	}
}

