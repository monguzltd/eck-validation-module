package eu.europeanainside.validation.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Container class for sets with processing status in XML format.
 * @author mbathori
 *
 */
@XmlRootElement(name = "sets")
public class SetListBean {
	
	List<String> set;

	/**
	 * Getter method for set attribute.
	 * @return list of sets
	 */
	public List<String> getSet() {
		return set;
	}
	
	/**
	 * Setter method for set attribute.
	 * @param profile list of sets
	 */
	@XmlElement
	public void setSet(List<String> set) {
		this.set = set;
	}
}
