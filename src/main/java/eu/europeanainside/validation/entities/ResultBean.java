package eu.europeanainside.validation.entities;

/**
 * Contains the validation's results, and the validation's status.
 * @author mbathori
 *
 */
public class ResultBean {
	
	private String resultXml;
	private String resultJson;
	private boolean successful;
	
	/**
	 * Getter method for resultXml attribute.
	 * @return result in XML format
	 */
	public String getResultXml() {
		return resultXml;
	}
	
	/**
	 * Setter method for resultXml attribute.
	 * @param resultXml result in XML format
	 */
	public void setResultXml(String resultXml) {
		this.resultXml = resultXml;
	}
	
	/**
	 * Getter method for successful attribute.
	 * @return validation's status
	 */
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 * Setter method for successful attribute.
	 * @param successful validation's status
	 */
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	/**
	 * Getter method for resultJson attribute.
	 * @return result in Json format.
	 */
	public String getResultJson() {
		return resultJson;
	}

	/**
	 * Setter method for resultJson attribute.
	 * @param resultJson result in Json format
	 */
	public void setResultJson(String resultJson) {
		this.resultJson = resultJson;
	}
	
	
}
