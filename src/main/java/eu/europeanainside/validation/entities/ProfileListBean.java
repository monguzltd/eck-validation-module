package eu.europeanainside.validation.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Container class for available profiles in XML format.
 * @author mbathori
 *
 */
@XmlRootElement(name = "profiles")
public class ProfileListBean {

	List<String> profile;

	/**
	 * Getter method for profile attribute.
	 * @return list of profiles
	 */
	public List<String> getProfile() {
		return profile;
	}
	
	/**
	 * Setter method for profile attribute.
	 * @param profile list of profiles
	 */
	@XmlElement
	public void setProfile(List<String> profile) {
		this.profile = profile;
	}
}
