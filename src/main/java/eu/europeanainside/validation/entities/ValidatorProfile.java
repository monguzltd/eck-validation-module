package eu.europeanainside.validation.entities;

/**
 * Entity class representing validator profiles.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class ValidatorProfile {

  private final String name;
  private final String body;

  /**
   * Constructor.
   * @param name The name of the profile.
   * @param body The body of the profile.
   */
  public ValidatorProfile(String name, String body) {
    super();
    this.name = name;
    this.body = body;
  }

  /**
   * Returns the name of the profile.
   * @return The profile name.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the body of the profile.
   * @return The profile body.
   */
  public String getBody() {
    return body;
  }

}
