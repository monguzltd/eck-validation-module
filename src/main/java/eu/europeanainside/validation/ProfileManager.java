package eu.europeanainside.validation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ProfileNotFoundException;
import eu.europeanainside.validation.utils.ConstantUtils;
import static eu.europeanainside.validation.ValidationConstants.*;

/**
 * Class responsible for profile management.
 * @author Katalin Szucs <kszucs@monguz.hu>
 */
public class ProfileManager {

  private static final String PROFILE_EXT_SUFFIX = ".profile";
  private static final String ECK_VALIDATION_PROFILE_HOME_PATH = ".eck/validation-profiles/";
  private static final Logger LOGGER = LoggerFactory.getLogger(ProfileManager.class);

  /**
   * Initialization steps.
   */
  public void init() {
    String home = System.getProperty(ConstantUtils.SYSTEM_PROPERTY_USER_HOME);
    Path userHome = Paths.get(home);

    Path profileDirPath = userHome.resolve(ECK_VALIDATION_PROFILE_HOME_PATH);
    if (!Files.exists(profileDirPath)) {
      LOGGER.info("Profile dir does not exist, creating.");
      try {
        Files.createDirectories(profileDirPath);
      } catch (IOException e) {
        LOGGER.error("Could not create directory for profiles!");
      }
    }
  }

  /**
   * Gets the provider based profile directory path.
   * @param providerName name of the provider
   * @return path of directory
   */
  public Path getProfileDirectoryPath(String providerName) {
    String home = System.getProperty(ConstantUtils.SYSTEM_PROPERTY_USER_HOME);
    Path userHome = Paths.get(home);

    return userHome.resolve(ECK_VALIDATION_PROFILE_HOME_PATH + providerName);
  }

  /**
   * Checks if the given provider directory exist in the profile directory, and creates it if it doesn't.
   * @param providerName name of the provider
   * @throws IOException If error occurs while creating directory
   */
  public void checkDirectory(String providerName) throws IOException {
    Path dirPath = getProfileDirectoryPath(providerName);
    if (!Files.exists(dirPath)) {
      LOGGER.info("Provider directory does not exist for provider {}, creating", providerName);
      try {
        Files.createDirectories(dirPath);
      } catch (IOException e) {
        LOGGER.error("Could not create directory for profiles!");
        throw new IOException("Could not create directory for profile: ", e.getCause());
      }
    }
  }

  /**
   * Provides a list of available profiles by name.
   * @param providerName provider name
   * @return List of the saved profiles.
   * @throws IOException If an IO exception occurs.
   */
  public List<ValidatorProfile> listProfiles(String providerName) throws IOException {
    List<ValidatorProfile> profiles = new ArrayList<>();
    Path profilePath = getProfileDirectoryPath(providerName);

    try (DirectoryStream<Path> dstream = Files.newDirectoryStream(profilePath, "*" + PROFILE_EXT_SUFFIX)) {
      for (Path filepath : dstream) {
        LOGGER.info("Reading profile: " + filepath);
        profiles.add(readProfile(filepath.toFile()));
      }
    } catch (ProfileNotFoundException ex) {
      throw new IOException("Failed to read profile.", ex);
    }

    return profiles;
  }

  /**
   * Returns a profile by name.
   * If the provider does not exist, tries to retrieve the same profile from the default provider.
   * @param profileName Profile name for identification.
   * @param providerName provider name
   * @return Profile body.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws IOException If an IO exception occurs while reading the profile.
   */
  public ValidatorProfile getProfile(String profileName, String providerName) throws ProfileNotFoundException,
    IOException {
    File profileFile = getProfileFile(profileName, providerName);
    if (!profileFile.exists()) {
      profileFile = getProfileFile(profileName, DEFAULT_PROVIDER);
      if (!profileFile.exists()) {
        LOGGER.error("Profile {} does not exist!", profileFile);
        throw new ProfileNotFoundException("The requested profile doesn't exist!");
      }
    }

    return readProfile(profileFile);
  }

  /**
   * Saves a new profile or updates an existing one.
   * @param profile Profile definition.
   * @param profileName Profile name for identification.
   * @param providerName provider name
   * @throws IOException If an exception occurs during the file save.
   */
  public void saveProfile(String profile, String profileName, String providerName) throws IOException {
    if (DEFAULT_PROVIDER.equals(providerName)) {
      LOGGER.warn("The default profiles are not allowed to be updated from the client side!");
      return;
    }
    checkDirectory(providerName);
    File profileFile = getProfileFile(profileName, providerName);
    try (FileWriter profileFileWriter = new FileWriter(profileFile, false)) {
      profileFileWriter.write(profile);
      profileFileWriter.flush();
    }
  }

  /**
   * Deletes a profile from the system.
   * @param profileName Profile name for identification.
   * @param providerName provider name
   * @throws IOException If the deletion fails.
   */
  public void deleteProfile(String profileName, String providerName) throws IOException {
    if (DEFAULT_PROVIDER.equals(providerName)) {
      LOGGER.warn("The default profiles are not allowed to be deleted from the client side!");
      return;
    }
    File profileFile = getProfileFile(profileName, providerName);
    if (profileFile.exists() && !profileFile.delete()) {
      LOGGER.error("Failed to delete profile {} for provider {}", profileName, providerName);
      throw new IOException("Failed to delete profile.");
    }
  }

  /**
   * Reads the profile from a File object into a String.
   * @param profileFile The File object to read the profile from.
   * @return The readed profile.
   * @throws ProfileNotFoundException If the profile can't be opened.
   */
  private ValidatorProfile readProfile(File profileFile) throws ProfileNotFoundException {
    try (Scanner profileScanner = new Scanner(profileFile)) {
      profileScanner.useDelimiter(ConstantUtils.FILE_EOF_REGEX);
      if (profileScanner.hasNext()) {
        String name = FilenameUtils.removeExtension(profileFile.getName());
        String body = profileScanner.next();
        return new ValidatorProfile(name, body);
      } else {
        LOGGER.error("No body found for profile {}", profileFile.getName());
        throw new ProfileNotFoundException("No body found for profile " + profileFile.getName());
      }
    } catch (FileNotFoundException e) {
      LOGGER.error("Profile {} does not exist!", profileFile.getAbsolutePath());
      throw new ProfileNotFoundException();
    }
  }

  /**
   * Returns a File object for the given profile name.
   * @param profileName Profile name for identification.
   * @param providerName provider name
   * @return The profile as a Java File object.
   * @throws IOException if the provider directory cannot be created
   */
  private File getProfileFile(String profileName, String providerName) throws IOException {
    if (!profileName.endsWith(PROFILE_EXT_SUFFIX)) {
      profileName = profileName + PROFILE_EXT_SUFFIX;
    }
    checkDirectory(providerName);
    return getProfileDirectoryPath(providerName).resolve(profileName).toFile();
  }
}
