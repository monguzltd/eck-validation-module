package eu.europeanainside.validation.api.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import eu.europeanainside.validation.api.BatchValidatorServices;
import eu.europeanainside.validation.api.ValidatorServices;
import eu.europeanainside.validation.entities.ProfileListBean;
import eu.europeanainside.validation.entities.ResultBean;
import eu.europeanainside.validation.entities.SetListBean;
import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ProfileNotFoundException;
import eu.europeanainside.validation.exception.ResultNotFoundException;
import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Controller class providing the REST API endpoints for profile and validation management.
 *
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
@Controller
@RequestMapping("Validation")
public final class ValidatorServicesController {

  @Autowired
  private ValidatorServices service;
  @Autowired
  private BatchValidatorServices batchService;
  private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorServices.class);
  private static final String NAME_KEY = "name";
  private static final String NAME_URL_PATTERN = "/{" + NAME_KEY + "}";
  private static final String PROVIDER_NAME_KEY = "provider";
  private static final String PROVIDER_NAME_URL_PATTERN = "/{" + PROVIDER_NAME_KEY + "}";
  private static final String SET_NUMBER_KEY = "setnumber";
  private static final String SET_NUMBER_NAME_URL_PATTERN = "/{" + SET_NUMBER_KEY + "}";
  private static final String GET_PROFILES = PROVIDER_NAME_URL_PATTERN + "/profiles";
  private static final String MANAGE_PROFILE = PROVIDER_NAME_URL_PATTERN + "/profiles" + NAME_URL_PATTERN;
  private static final String SINGLE_VALIDATE = PROVIDER_NAME_URL_PATTERN + "/single/validate" + NAME_URL_PATTERN;
  private static final String GET_SETS_WITH_PROCESSING_STATUS = PROVIDER_NAME_URL_PATTERN + "/sets";
  private static final String BATCH_VALIDATION = PROVIDER_NAME_URL_PATTERN + SET_NUMBER_NAME_URL_PATTERN + "/validate" + NAME_URL_PATTERN;
  private static final String BATCH_VALIDATION_RESULT = PROVIDER_NAME_URL_PATTERN + SET_NUMBER_NAME_URL_PATTERN + "/result";

  /**
   * Exception handler for validation errors.
   *
   * @param ex Exception handled.
   * @return error message.
   */
  @ExceptionHandler(ValidationException.class)
  @ResponseBody
  public String handleValidationException(ValidationException ex) {
    LOGGER.error("There were errors while validating the record.", ex);
    return "The record is not valid! Error message: " + ex.getCause().getMessage();
  }

  /**
   * Provides a list of available profiles for the requested provider in JSON format.
   *
   * @param providerName name of the provider
   * @return list of available profiles
   * @throws IOException If an IO exception occurs while getting profiles name.
   */
  @RequestMapping(value = GET_PROFILES, method = RequestMethod.GET, produces = "application/json")
  @ResponseBody
  public List<String> listProfilesAsJson(@PathVariable(PROVIDER_NAME_KEY) String providerName) throws IOException {
    List<ValidatorProfile> profiles = service.listProfiles(providerName);
    List<String> result = new ArrayList<String>();
    for (ValidatorProfile profile : profiles) {
      result.add(profile.getName());
    }
    return result;
  }

  /**
   * Provides a list of available profiles for the requested provider in XML format.
   *
   * @param providerName name of the provider
   * @return list of available profiles
   * @throws IOException If an IO exception occurs while getting profiles name.
   */
  @RequestMapping(value = GET_PROFILES, method = RequestMethod.GET, produces = "application/xml")
  @ResponseBody
  public ProfileListBean listProfilesAsXml(@PathVariable(PROVIDER_NAME_KEY) String providerName) throws IOException {
    List<ValidatorProfile> profiles = service.listProfiles(providerName);
    List<String> result = new ArrayList<String>();
    for (ValidatorProfile profile : profiles) {
      result.add(profile.getName());
    }
    ProfileListBean list = new ProfileListBean();
    list.setProfile(result);

    return list;
  }

  /**
   * Returns a profile by name.
   *
   * @param profileName Profile name for identification.
   * @return Profile body.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws IOException If an IO exception occurs while reading the profile.
   */
  @RequestMapping(value = MANAGE_PROFILE, method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<String> getProfile(@PathVariable(PROVIDER_NAME_KEY) String providerName, @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException,
    IOException {
    return new ResponseEntity<>(service.getProfile(profileName, providerName).getBody(), HttpStatus.OK);
  }

  /**
   * Saves a new profile or updates an existing one (provided in the request body).
   *
   * @param profile The profile to save.
   * @param profileName Profile name for identification.
   * @throws IOException If an IO exception occurs during profile save.
   */
  @RequestMapping(value = MANAGE_PROFILE, method = {RequestMethod.PUT, RequestMethod.POST},
    consumes = MediaType.TEXT_PLAIN_VALUE)
  public ResponseEntity<Void> saveProfile(@RequestBody String profile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName, @PathVariable(NAME_KEY) String profileName)
    throws IOException {
    service.saveProfile(profile, profileName, providerName);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = MANAGE_PROFILE, method = {RequestMethod.PUT, RequestMethod.POST},
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Void> multipartSaveProfile(@RequestBody MultipartFile profile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName, @PathVariable(NAME_KEY) String profileName)
    throws IOException, ValidationException {
    String fileContent;

    if (!profile.isEmpty()) {
      byte[] bytes = profile.getBytes();
      fileContent = new String(bytes);
    } else {
      throw new ValidationException("Received profile file is empty!");
    }

    service.saveProfile(fileContent, profileName, providerName);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = MANAGE_PROFILE, method = {RequestMethod.PUT, RequestMethod.POST})
  public ResponseEntity<Void> defaultMultipartSaveProfile(@RequestBody MultipartFile profile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName, @PathVariable(NAME_KEY) String profileName)
    throws IOException, ValidationException {
    String fileContent;

    if (!profile.isEmpty()) {
      byte[] bytes = profile.getBytes();
      fileContent = new String(bytes);
    } else {
      throw new ValidationException("Received profile file is empty!");
    }

    service.saveProfile(fileContent, profileName, providerName);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Deletes a profile from the system.
   *
   * @param profileName Profile name for identification.
   * @throws IOException If an IO exception occurs while deleting the profile.
   */
  @RequestMapping(value = MANAGE_PROFILE, method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteProfile(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws IOException {
    service.deleteProfile(profileName, providerName);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Validates the given record with the selected profile.
   *
   * @param record The record.
   * @param providerName name of the provider
   * @param profileName Profile name for identification.
   * @return Result of the validation.
   * @throws IOException If an IO exception occurs while getting the profile.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws ValidationException If error occurs while validation process.
   */
  @RequestMapping(value = SINGLE_VALIDATE, method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_XML_VALUE, produces = "application/xml")
  @ResponseBody
  public ResponseEntity<String> singleValidateAsXml(@RequestBody String record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException, IOException, ValidationException {
    ResultBean result = service.validateWithProfile(record, profileName, providerName);

    if (result.isSuccessful()) {
      return new ResponseEntity<>(result.getResultXml(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getResultXml(), HttpStatus.PRECONDITION_FAILED);
    }
  }

  @RequestMapping(value = SINGLE_VALIDATE, method = RequestMethod.POST,
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/xml")
  @ResponseBody
  public ResponseEntity<String> multipartSingleValidateAsXml(@RequestBody MultipartFile record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException, IOException, ValidationException {
    String fileContent;

    if (!record.isEmpty()) {
      byte[] bytes = record.getBytes();
      fileContent = new String(bytes);
    } else {
      throw new ValidationException("Received record file is empty!");
    }

    ResultBean result = service.validateWithProfile(fileContent, profileName, providerName);

    if (result.isSuccessful()) {
      return new ResponseEntity<>(result.getResultXml(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getResultXml(), HttpStatus.PRECONDITION_FAILED);
    }
  }

  @RequestMapping(value = SINGLE_VALIDATE, method = RequestMethod.POST,
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
  @ResponseBody
  public ResponseEntity<String> multipartSingleValidateAsJson(@RequestBody MultipartFile record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException, IOException, ValidationException {
    String fileContent;

    if (!record.isEmpty()) {
      byte[] bytes = record.getBytes();
      fileContent = new String(bytes);
    } else {
      throw new ValidationException("Received record file is empty!");
    }

    ResultBean result = service.validateWithProfile(fileContent, profileName, providerName);

    if (result.isSuccessful()) {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.PRECONDITION_FAILED);
    }
  }

  @RequestMapping(value = SINGLE_VALIDATE, method = RequestMethod.POST,
    produces = "application/json")
  @ResponseBody
  public ResponseEntity<String> defaultMultipartSingleValidateAsJson(@RequestBody MultipartFile record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException, IOException, ValidationException {
    String fileContent;

    if (!record.isEmpty()) {
      byte[] bytes = record.getBytes();
      fileContent = new String(bytes);
    } else {
      throw new ValidationException("Received record file is empty!");
    }

    ResultBean result = service.validateWithProfile(fileContent, profileName, providerName);

    if (result.isSuccessful()) {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.PRECONDITION_FAILED);
    }
  }

  /**
   * Validates the given record with the selected profile.
   *
   * @param record The record.
   * @param providerName name of the provider
   * @param profileName Profile name for identification.
   * @return Result of the validation in Json format.
   * @throws IOException If an IO exception occurs while getting the profile.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws ValidationException If error occurs while validation process.
   */
  @RequestMapping(value = SINGLE_VALIDATE, method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_XML_VALUE, produces = "application/json")
  @ResponseBody
  public ResponseEntity<String> singleValidateAsJson(@RequestBody String record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName) throws ProfileNotFoundException, IOException, ValidationException {
    ResultBean result = service.validateWithProfile(record, profileName, providerName);

    if (result.isSuccessful()) {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(result.getResultJson(), HttpStatus.PRECONDITION_FAILED);
    }
  }

  /**
   * Unzip the given zip file and validates the records with the selected profile.
   *
   * @param zipFile The zip file.
   * @param providerName name of the provider
   * @param profileName Profile name for identification.
   * @param setNumber identification number of the validation
   * @throws IOException If an IO exception occurs while getting the profile.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws ValidationException If error occurs while validation.
   */
  @RequestMapping(value = BATCH_VALIDATION, method = RequestMethod.POST,
    consumes = "application/zip")
  public ResponseEntity<Void> validateZipWithProfile(@RequestBody byte[] zipFile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName,
    @PathVariable(SET_NUMBER_KEY) String setNumber)
    throws ProfileNotFoundException, IOException, ValidationException {
    List<String> recordList;
    try (ByteArrayInputStream zipInputStream = new ByteArrayInputStream(zipFile)) {
      recordList = getRecordsFromBundle(zipInputStream);
    }
    if (batchService.validateBatchWithProfile(recordList, profileName, providerName, setNumber)) {
      return new ResponseEntity<>(HttpStatus.ACCEPTED);
    } else {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
  }

  @RequestMapping(value = BATCH_VALIDATION, method = RequestMethod.POST,
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Void> multipartValidateZipWithProfile(@RequestBody MultipartFile zipFile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName,
    @PathVariable(SET_NUMBER_KEY) String setNumber)
    throws ProfileNotFoundException, IOException, ValidationException {
    List<String> recordList = getRecordsFromBundle(zipFile.getInputStream());
    if (batchService.validateBatchWithProfile(recordList, profileName, providerName, setNumber)) {
      return new ResponseEntity<>(HttpStatus.ACCEPTED);
    } else {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
  }

  @RequestMapping(value = BATCH_VALIDATION, method = RequestMethod.POST)
  public ResponseEntity<Void> defaultMultipartBatchValidate(@RequestBody MultipartFile zipFile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(NAME_KEY) String profileName,
    @PathVariable(SET_NUMBER_KEY) String setNumber)
    throws ProfileNotFoundException, IOException, ValidationException {
    List<String> recordList = getRecordsFromBundle(zipFile.getInputStream());
    if (batchService.validateBatchWithProfile(recordList, profileName, providerName, setNumber)) {
      return new ResponseEntity<>(HttpStatus.ACCEPTED);
    } else {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
  }

  /**
   * Gets the list of sets with processing status in JSON format.
   *
   * @param providerName name of the provider
   * @return list of sets
   */
  @RequestMapping(value = GET_SETS_WITH_PROCESSING_STATUS, method = RequestMethod.GET, produces = "application/json")
  @ResponseBody
  public List<String> getOngoingValidationsAsJson(@PathVariable(PROVIDER_NAME_KEY) String providerName) {
    return batchService.getOngoingValidations(providerName);
  }

  /**
   * Gets the list of sets with processing status in XML format.
   *
   * @param providerName name of the provider
   * @return list of sets
   * @throws ResultXmlBuildingException If error occurs while validation process.
   */
  @RequestMapping(value = GET_SETS_WITH_PROCESSING_STATUS, method = RequestMethod.GET, produces = "application/xml")
  @ResponseBody
  public SetListBean getOngoingValidationsAsXml(@PathVariable(PROVIDER_NAME_KEY) String providerName) throws ResultXmlBuildingException {
    SetListBean result = new SetListBean();
    List<String> setList = batchService.getOngoingValidations(providerName);
    result.setSet(setList);
    return result;
  }

  /**
   * Gets the given set's validation result in Json format.
   *
   * @param providerName name of the provider
   * @param setNumber sets identifier
   * @return validations result
   * @throws ResultNotFoundException If the requested set doesn't exist.
   */
  @RequestMapping(value = BATCH_VALIDATION_RESULT, method = RequestMethod.GET, produces = "application/json")
  @ResponseBody
  public ResponseEntity<String> getValidationResultAsJson(
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(SET_NUMBER_KEY) String setNumber) throws ResultNotFoundException {
    try {
      String result = batchService.getValidationResultAsJson(setNumber, providerName);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (ValidationException e) {
      return new ResponseEntity<>(HttpStatus.PROCESSING);
    }
  }

  /**
   * Gets the given set's validation result in XML format.
   *
   * @param providerName name of the provider
   * @param setNumber sets identifier
   * @return validations result
   * @throws ResultNotFoundException If the requested set doesn't exist.
   */
  @RequestMapping(value = BATCH_VALIDATION_RESULT, method = RequestMethod.GET, produces = "application/xml")
  @ResponseBody
  public ResponseEntity<String> getValidationResultAsXml(
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(SET_NUMBER_KEY) String setNumber) throws ResultNotFoundException {
    try {
      String result = batchService.getValidationResult(setNumber, providerName);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (ValidationException e) {
      return new ResponseEntity<>(HttpStatus.PROCESSING);
    }
  }

  /**
   * Gets the given set's validation result in zip format.
   *
   * @param providerName name of the provider
   * @param setNumber sets identifier
   * @return validations result
   * @throws ResultNotFoundException If the requested set doesn't exist.
   */
  @RequestMapping(value = BATCH_VALIDATION_RESULT, method = RequestMethod.GET, produces = "application/zip")
  @ResponseBody
  public ResponseEntity<byte[]> getValidationResultAsZip(
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(SET_NUMBER_KEY) String setNumber) throws ResultNotFoundException {
    try {
      byte[] result = batchService.getValidationResultAsZip(setNumber, providerName);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (ValidationException e) {
      return new ResponseEntity<>(HttpStatus.PROCESSING);
    }
  }

  private List<String> getRecordsFromBundle(InputStream zipInputStream) throws IOException {
    List<String> recordList = new ArrayList<>();
    byte[] buffer = new byte[1024];
    try (ZipInputStream zis = new ZipInputStream(zipInputStream)) {
      ZipEntry zipEntry = zis.getNextEntry();
      while (zipEntry != null) {
        int len;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
          while ((len = zis.read(buffer)) > 0) {
            bos.write(buffer, 0, len);
          }
          String xmlContent = bos.toString("UTF-8");
          recordList.add(xmlContent);
          zipEntry = zis.getNextEntry();
        }
      }
      zis.closeEntry();
    }
    return recordList;
  }
}
