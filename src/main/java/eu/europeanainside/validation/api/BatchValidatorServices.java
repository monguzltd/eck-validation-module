package eu.europeanainside.validation.api;

import java.io.IOException;
import java.util.List;

import eu.europeanainside.validation.BatchValidator;
import eu.europeanainside.validation.ProfileManager;
import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ProfileNotFoundException;
import eu.europeanainside.validation.exception.ResultNotFoundException;
import eu.europeanainside.validation.exception.ValidationException;

/**
 *
 * @author Gabor Nemeth
 */
public class BatchValidatorServices {

  private ProfileManager profileManager;
  private BatchValidator batchValidator;

  public void setProfileManager(ProfileManager profileManager) {
    this.profileManager = profileManager;
  }

  public void setBatchValidator(BatchValidator batchValidator) {
    this.batchValidator = batchValidator;
  }

  /**
   * Validates the given records with the selected profile with in new thread.
   *
   * @param record The record.
   * @param profileName Profile name for identification.
   * @return Result of the validation.
   * @throws IOException If the profile load fails.
   * @throws ProfileNotFoundException If the profile is not found.
   */
  public boolean validateBatchWithProfile(List<String> records, String profileName, String providerName, String setNumber)
          throws ProfileNotFoundException, IOException {
    ValidatorProfile profile = profileManager.getProfile(profileName, providerName);
    return batchValidator.validate(records, profile, providerName, setNumber);
  }
  
  /**
   * Lists the sets with processing status.
   * @param providerName name of the provider
   * @return list of sets
   */
  public List<String> getOngoingValidations(String providerName) {
	    return batchValidator.getOngoingValidations(providerName);
	  }

  /**
   * Gets the validation result.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as String
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public String getValidationResult(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
    return batchValidator.getValidationResult(setNumber, providerName);
  }
  
  /**
   * Gets the validation result as Zip.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as Zip
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public byte[] getValidationResultAsZip(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
	    return batchValidator.getValidationResultAsZip(setNumber, providerName);
	  }
  
  /**
   * Gets the validation result as Json.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as Json
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public String getValidationResultAsJson(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
	    return batchValidator.getValidationResultAsJson(setNumber, providerName);
	  }
}
