package eu.europeanainside.validation.api;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static eu.europeanainside.validation.ValidationConstants.*;
import eu.europeanainside.validation.ProfileManager;
import eu.europeanainside.validation.Validator;
import eu.europeanainside.validation.entities.ResultBean;
import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ProfileNotFoundException;
import eu.europeanainside.validation.exception.ValidationException;

/**
 * Service provider class for exposing validator services to the REST API and internal usages.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class ValidatorServices {

  private ProfileManager profileManager;
  private Validator validator;
  private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorServices.class);

  /**
   * Provides a list of available profiles by name.
   * @return List of the saved profiles.
   * @throws IOException If an IO exception occurs.
   */
  public List<ValidatorProfile> listProfiles(String providerName) throws IOException {
    return profileManager.listProfiles(providerName);
  }

  /**
   * Returns a profile by name.
   * @param profileName Profile name for identification.
   * @return Profile body.
   * @throws ProfileNotFoundException If the profile does not exist.
   * @throws IOException If an IO exception occurs while reading the profile.
   */
  public ValidatorProfile getProfile(String profileName, String providerName) throws ProfileNotFoundException, IOException {
    return profileManager.getProfile(profileName, providerName);
  }

  /**
   * Saves a new profile or updates an existing one.
   * @param profile Profile definition.
   * @param profileName Profile name for identification.
   * @throws IOException If an exception occurs during the file save.
   */
  public void saveProfile(String profile, String profileName, String providerName) throws IOException {
    profileManager.saveProfile(profile, profileName, providerName);
  }

  /**
   * Deletes a profile from the system.
   * @param profileName Profile name for identification.
   * @throws IOException If the deletion fails.
   */
  public void deleteProfile(String profileName, String providerName) throws IOException {
    profileManager.deleteProfile(profileName, providerName);
  }

  /**
   * Validates the given record with the selected profile.
   * @param record The record.
   * @param profileName Profile name for identification.
   * @return Result of the validation.
   * @throws IOException If the profile load fails.
   * @throws ProfileNotFoundException If the profile is not found.
   * @throws ValidationException
   */
  public ResultBean validateWithProfile(String record, String profileName, String providerName) throws ProfileNotFoundException,
    IOException, ValidationException {
    ValidatorProfile profile;

    try {
      profile = profileManager.getProfile(profileName, providerName);
    } catch (ProfileNotFoundException e) {
      LOGGER.debug("The requested profile doesn't exist, default profile is going to be used!");
      profile = profileManager.getProfile(DEFAULT_PROFILE, DEFAULT_PROVIDER);
    }
    return validator.validateWithProfile(record, profile);
  }

  /**
   * Setter for ProfileManager.
   * @param profileManager The new ProfileManager.
   */
  public void setProfileManager(ProfileManager profileManager) {
    this.profileManager = profileManager;
  }

  /**
   * Setter for Validator.
   * @param validator The new Validator.
   */
  public void setValidator(Validator validator) {
    this.validator = validator;
  }
}
