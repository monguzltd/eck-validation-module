package eu.europeanainside.validation.xmlbuilders;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;

@XStreamAlias("error")
@XStreamConverter(value = ToAttributedValueConverter.class, strings = { "errorMessage" })
public class ValidationError {

  @XStreamAsAttribute
  @XStreamAlias("plugin")
  private String plugin;
  @SuppressWarnings("unused") //used at serialization
  private String errorMessage;

  public ValidationError(String plugin, String errorMessage) {
    this.plugin = plugin;
    this.errorMessage = errorMessage;
  }
}
