package eu.europeanainside.validation.xmlbuilders;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("validationresult")
public class ValidationResult {

  @XStreamImplicit
  List<RecordResult> results = new ArrayList<>();

  public void addNewResult(String id) {
    results.add(new RecordResult(id));
  }

  public RecordResult retrieveRecordWithId(String id) {
    for (RecordResult record : results) {
      if (record.getId().equals(id)) {
        return record;
      }
    }
    RecordResult newResult = new RecordResult(id);
    results.add(newResult);
    return newResult;
  }

}
