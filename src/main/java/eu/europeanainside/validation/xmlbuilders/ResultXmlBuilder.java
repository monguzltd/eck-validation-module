package eu.europeanainside.validation.xmlbuilders;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import net.sf.saxon.dom.DocumentBuilderImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.utils.ResultDirectoryUtils;

/**
 * This class is responsible for building the validation result XML file.
 * @author mbathori
 */
public class ResultXmlBuilder {

  private ValidationResult result = new ValidationResult();
  private String xmlRecordIdTag;
  private boolean hasErrors = false;

  /**
   * Default constructor that builds the base of the result document.
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public ResultXmlBuilder(String xmlRecordIdTag) throws ResultXmlBuildingException {
    this.xmlRecordIdTag = xmlRecordIdTag;
  }

  /**
   * Builds the base of the result document, and creates the result directory for the given provider, if it doesn't
   * exist.
   * @param providerName name of the provider
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public ResultXmlBuilder(String providerName, String xmlRecordIdTag) throws ResultXmlBuildingException {
    createResultFolder(providerName);
    this.xmlRecordIdTag = xmlRecordIdTag;
  }

  /**
   * Creates the result directory for the given provider, if it doesn't exist.
   * @param providerName name of the provider
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void createResultFolder(String providerName) throws ResultXmlBuildingException {
    Path resultDirPath = ResultDirectoryUtils.getResultDirecotyPath(providerName);
    if (!Files.exists(resultDirPath)) {
      try {
        Files.createDirectories(resultDirPath);
      } catch (IOException e) {
        throw new ResultXmlBuildingException("Could not create directory for profiles!");
      }
    }
  }

  /**
   * Gets the records name from the given XML.
   * @param xmlRecord XML file
   * @return name of the record or null if the node doesn't exist
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public String getRecordName(String xmlRecord) throws ResultXmlBuildingException {

    try {
      DocumentBuilder builder = new DocumentBuilderImpl();
      Document document = builder.parse(new InputSource(new StringReader(xmlRecord)));

      Element rootElement = document.getDocumentElement();

      NodeList list = rootElement.getElementsByTagName(xmlRecordIdTag);

      if (list != null && list.getLength() > 0) {
        NodeList subList = list.item(0).getChildNodes();

        if (subList != null && subList.getLength() > 0) {
          return subList.item(0).getNodeValue();
        }
      }
    } catch (SAXException | IOException e) {
      throw new ResultXmlBuildingException("Failed to get record name: " + e.getCause());
    }
    return null;

  }

  /**
   * Adds an XSD validation error row to the result XML file.
   * @param xmlRecord XML file
   * @param errors list of XSD validation errors
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addXSDValidationError(String xmlRecord, List<Exception> errors) throws ResultXmlBuildingException {
    RecordResult record = result.retrieveRecordWithId(getRecordName(xmlRecord));
    for (Exception exception : errors) {
      record.addError("xsdValidator", exception.getMessage());
    }
    hasErrors = true;
  }

  /**
   * Adds an URI validation error row to the result XML file.
   * @param xmlRecord XML file
   * @param error URI validation error message
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addURIValidationError(String xmlRecord, String error) throws ResultXmlBuildingException {
    addValidationError(xmlRecord, error, "uriValidator");
  }

  /**
   * Adds a Link validation error row to the result XML file.
   * @param xmlRecord XML file
   * @param error Link validation error message
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addLinkValidationError(String xmlRecord, String error) throws ResultXmlBuildingException {
    addValidationError(xmlRecord, error, "linkValidator");
  }

  /**
   * Adds a Resource validation error row to the result XML file.
   * @param xmlRecord XML file
   * @param error Resource validation error message
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addResourceValidationError(String xmlRecord, String error) throws ResultXmlBuildingException {
    addValidationError(xmlRecord, error, "resourceValidator");
  }

  /**
   * Adds a Schematron validation error row to the result XML file.
   * @param xmlRecord XML file
   * @param error Schematron validation error message
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addSchematronValidationError(String xmlRecord, String error) throws ResultXmlBuildingException {
    addValidationError(xmlRecord, error, "schematronValidator");
  }

  private void addValidationError(String xmlRecord, String error, String pluginName) throws ResultXmlBuildingException {
    RecordResult record = result.retrieveRecordWithId(getRecordName(xmlRecord));
    record.addError(pluginName, error);
    hasErrors = true;
  }

  /**
   * Adds a successful validation row to the result XML file.
   * @param xmlRecord XML file
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void addNewRecord(String xmlRecord) throws ResultXmlBuildingException {
    result.addNewResult(getRecordName(xmlRecord)); //will create empty successful record where nonexistent
  }

  /**
   * Writes to file the result XML.
   * @param name name of the file
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public void writeToFile(String providerName, String name) throws ResultXmlBuildingException {
    try {
      XStream xstream = new XStream();
      xstream.autodetectAnnotations(true);
      xstream.toXML(result, new FileOutputStream(new File(ResultDirectoryUtils.getResultDirecotyPath(providerName).toString() + "/" + name)));
    } catch (FileNotFoundException e) {
      throw new ResultXmlBuildingException("Failed to write result xml into file: " + e.getCause());
    }
  }

  /**
   * Splits the give XML file into lido records.
   * @param xmlRecords list of XML files
   * @return list of lido records
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public List<String> splitRecords(List<String> xmlRecords, String xmlRecordStartTag) throws ResultXmlBuildingException {
    List<String> records = new ArrayList<String>();

    try {
      DocumentBuilder builder = new DocumentBuilderImpl();
      for (String xmlRecord : xmlRecords) {
        Document document = builder.parse(new InputSource(new StringReader(xmlRecord)));

        NodeList nodeList = document.getElementsByTagName(xmlRecordStartTag);

        for (int i = 0; i < nodeList.getLength(); i++) {
          Node node = nodeList.item(i);
          records.add(nodeToString(node));
        }
      }
    } catch (SAXException | IOException e) {
      throw new ResultXmlBuildingException("Failed to to split xml records: " + e.getCause());
    }
    return records;
  }

  /**
   * Transform an XML node to String.
   * @param node XML node
   * @return node as string
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public String nodeToString(Node node) throws ResultXmlBuildingException {
    StringWriter stringWriter = new StringWriter();
    try {
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      transformer.transform(new DOMSource(node), new StreamResult(stringWriter));
    } catch (TransformerException e) {
      throw new ResultXmlBuildingException("Failed to transform node to String: " + e.getCause());
    }
    return stringWriter.toString();
  }

  /**
   * Transform the result document to String.
   * @return result document as string
   * @throws ResultXmlBuildingException if error occurs while XML building process
   */
  public String documentToString() throws ResultXmlBuildingException {
    XStream xstream = new XStream();
    xstream.autodetectAnnotations(true);
    return xstream.toXML(result);
  }
  
  public boolean hasErrors() {
    return hasErrors;
  }
}
