package eu.europeanainside.validation.xmlbuilders;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("record")
public class RecordResult {

  @XStreamAsAttribute
  private String id;
  @XStreamAsAttribute
  private String result;
  @XStreamImplicit
  private List<ValidationError> errors = new ArrayList<>();

  public RecordResult(String id) {
    this.id = id;
    this.result = "successful";
  }

  public void addError(String plugin, String errorMessage) {
    errors.add(new ValidationError(plugin, errorMessage));
    result = "error";
  }

  public String getId() {
    return id;
  }

}
