package eu.europeanainside.validation.utils;

/**
 * This class contains constant values that are used in multiple classes.
 * @author mbathori
 *
 */
public final class ConstantUtils {
	
	private ConstantUtils(){
		
	}

	/**
	 * Finished validation status.
	 */
	public static final String VALIDATION_OK_STATUS = "OK";
	/**
	 * In progress validation status.
	 */
	public static final String VALIDATION_PROCESSING_STATUS = "Validation in progress!";
	/**
	 * User home.
	 */
	public static final String SYSTEM_PROPERTY_USER_HOME = "user.home";
	/**
	 * File eof regular expression.
	 */
	public static final String FILE_EOF_REGEX = "\\Z";
	/**
	 * XML file suffix.
	 */
	public static final String XML_FILE_SUFFIX = ".xml";
	
	public static final String DEFAULT_PROVIDER = "default";
	
	public static final String DEFAULT_PROFILE = "lido";

}
