package eu.europeanainside.validation.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltTransformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class handling XSLT transformation.
 * @author Agoston Berger <aberger@monguz.hu>
 *
 */
public final class Transformer {

  private static final Logger LOGGER = LoggerFactory.getLogger(Transformer.class);

  /** No instances of this. */
  private Transformer() {
    // no
  }

  /**
   * Performs XSLT transformation.
   * @param transformerResource XSLT input stream
   * @param input input XML as stream
   * @return transformed XML as stream
   * @throws IOException if an I/O error occurs
   * @throws TransformerException if an XSLT error occurs
   * @throws SaxonApiException general Saxon error
   */
  public static ByteArrayOutputStream transform(InputStream transformerResource, InputStream input)
      throws IOException, TransformerException, SaxonApiException {
    LOGGER.debug("Starting transformation.");
    try (ByteArrayOutputStream output = new ByteArrayOutputStream(); OutputStreamWriter writer = new OutputStreamWriter(output)) {
      Processor processor = new Processor(false);
      XsltCompiler compiler = processor.newXsltCompiler();
      XsltTransformer transformer = compiler.compile(new StreamSource(transformerResource)).load();
      transformer.setSource(new StreamSource(input));
      Serializer out = new Serializer();
      out.setOutputProperty(Serializer.Property.METHOD, "xml");
      out.setOutputProperty(Serializer.Property.ENCODING, "UTF-8");
      out.setOutputWriter(writer);
      transformer.setDestination(out);
      transformer.transform();
      LOGGER.debug("Transformation finished.");
      return output;
    }
  }

}
