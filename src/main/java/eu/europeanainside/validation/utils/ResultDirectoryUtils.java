package eu.europeanainside.validation.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import eu.europeanainside.validation.exception.ResultNotFoundException;
import eu.europeanainside.validation.exception.ValidationException;

/**
 * Util class for directory based actions.
 * @author mbathori
 *
 */
public final class ResultDirectoryUtils {

  private static final String RESULT_XML_FILE_NAME = "result" + ConstantUtils.XML_FILE_SUFFIX;
  private static final String ECK_VALIDATION_RESULT_HOME_PATH = ".eck/validation-result/";

  /**
   * Gets the results base directory path.
   * @return path of directory
   */
  public static Path getBaseResultDirecotyPath() {
    String home = System.getProperty(ConstantUtils.SYSTEM_PROPERTY_USER_HOME);
    Path userHome = Paths.get(home);

    return userHome.resolve(ECK_VALIDATION_RESULT_HOME_PATH);
  }

  /**
   * Gets the provider based result directory path.
   * @param providerName name of the provider
   * @return path of directory
   */
  public static Path getResultDirecotyPath(String providerName) {//TODO: refactor
    String home = System.getProperty(ConstantUtils.SYSTEM_PROPERTY_USER_HOME);
    Path userHome = Paths.get(home);

    return userHome.resolve(ECK_VALIDATION_RESULT_HOME_PATH + providerName);
  }

  /**
   * Gets the requested result file in zip format.
   * @param resultXml result XML file
   * @return result in zip file
   * @throws ValidationException If error occurs while creating the zipped result.
   * @throws ResultNotFoundException
   */
  public static byte[] getResultAsZip(String setNumber, String providerName)
    throws ValidationException, ResultNotFoundException {
    String resultXml = getResultFile(setNumber, providerName);
    byte[] result = null;


    try (ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
      ZipOutputStream zipStream = new ZipOutputStream(temporaryStream)) {
      ZipEntry previewEntry = new ZipEntry(RESULT_XML_FILE_NAME);
      zipStream.putNextEntry(previewEntry);
      zipStream.write(resultXml.getBytes(Charset.forName("UTF-8")));
      zipStream.closeEntry();
      zipStream.flush();
      result = temporaryStream.toByteArray();
    } catch (IOException exc) {
      throw new ValidationException(
        "Failed to create result ZIP stream contents.", exc);
    }
    return result;
  }

  /**
   * Gets the requested result file in Json format.
   * @param resultXml result XML file
   * @return result in Json file
   * @throws ValidationException If error occurs while creating the Json result.
   * @throws ResultNotFoundException If the requested file doesn't exist.
   */
  public static String getResultAsJson(String setNumber, String providerName)
    throws ValidationException, ResultNotFoundException {
    String resultXml = getResultFile(setNumber, providerName);
    return xmlToJson(resultXml);
  }

  /**
   * Parse XML to Json.
   * @param xml input XML
   * @return requested XML in Json format
   * @throws ValidationException If error occurs while parsing XML to Json
   */
  public static String xmlToJson(String xml) throws ValidationException {
    try {
      JSONObject xmlJSONObj = XML.toJSONObject(xml);
      String jsonString = xmlJSONObj.toString();
      return jsonString;
    } catch (JSONException e) {
      throw new ValidationException("Error occured while parsing result to Json!" + e.getCause());
    }
  }

  /**
   * Gets the result file from the container directory.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file
   * @throws ResultNotFoundException If the file doesn't exist.
   */
  public static String getResultFile(String setNumber, String providerName)
    throws ResultNotFoundException {

    Path resultDirPath = getResultDirecotyPath(providerName);
    File resultFile = resultDirPath.resolve(setNumber + ConstantUtils.XML_FILE_SUFFIX).toFile();

    try (Scanner profileScanner = new Scanner(resultFile)) {
      profileScanner.useDelimiter(ConstantUtils.FILE_EOF_REGEX);
      if (profileScanner.hasNext()) {
        return profileScanner.next();
      } else {
        throw new ResultNotFoundException(
          "No content found for reult: " + resultFile.getName());
      }
    } catch (FileNotFoundException e) {
      throw new ResultNotFoundException("Result file doesn't exist!");
    }
  }
}
