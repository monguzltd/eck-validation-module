package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 *
 * @author kszucs
 */
public class SchematronValidatorPluginHandler extends NamespaceHandlerSupport {

  @Override
  public void init() {
    registerBeanDefinitionParser("define", new SchematronValidatorDefinitionParser());
  }
}
