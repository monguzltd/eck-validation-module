package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class URIValidatorPluginHandler extends NamespaceHandlerSupport {

  @Override
  public void init() {
    registerBeanDefinitionParser("define", new URIValidatorPluginDefinitionParser());
  }

}
