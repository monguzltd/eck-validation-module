package eu.europeanainside.validation.namespacehandler;

import eu.europeanainside.validation.plugins.LinkValueValidator;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 *
 * @author kszucs
 */
public class LinkValueValidatorPluginDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition linkValueValidator =
      new RootBeanDefinition(LinkValueValidator.class);
    String linkXpath = element.getAttribute("linkXpath");
    linkValueValidator.getPropertyValues().add("linkXpath", linkXpath);
    String validUrls = element.getAttribute("validUrls");
    linkValueValidator.getPropertyValues().add("validUrls", validUrls);
    parserContext.registerBeanComponent(new BeanComponentDefinition(linkValueValidator, element.getAttribute("name")));

    return null;
  }
}
