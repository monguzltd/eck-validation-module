package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class LinkCheckerPluginHandler extends NamespaceHandlerSupport {
  @Override
  public void init() {
    registerBeanDefinitionParser("define", new LinkCheckerPluginDefinitionParser());
  }
}
