package eu.europeanainside.validation.namespacehandler;

import eu.europeanainside.validation.plugins.SchematronValidatorPlugin;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 *
 * @author kszucs
 */
public class SchematronValidatorDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition schematronValidator =
      new RootBeanDefinition(SchematronValidatorPlugin.class);
    String rulesetLocation = element.getAttribute("rulesetLocation");
    schematronValidator.getPropertyValues().add("rulesetLocation", rulesetLocation);
    String preTransformPath = element.getAttribute("preTransformPath");
    schematronValidator.getPropertyValues().add("preTransformPath", preTransformPath);
    parserContext.registerBeanComponent(new BeanComponentDefinition(schematronValidator, element.getAttribute("name")));

    return null;
  }
}
