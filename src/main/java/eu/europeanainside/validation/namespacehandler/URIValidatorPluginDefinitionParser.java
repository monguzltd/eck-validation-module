package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import eu.europeanainside.validation.plugins.URIValidator;

public class URIValidatorPluginDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition uriValidator =
      new RootBeanDefinition(URIValidator.class);
    String tagName = element.getAttribute("tagName");
    uriValidator.getPropertyValues().add("tagName", tagName);
    String attributeName = element.getAttribute("attributeName");
    uriValidator.getPropertyValues().add("attributeName", attributeName);
    parserContext.registerBeanComponent(new BeanComponentDefinition(uriValidator, element.getAttribute("name")));

    return null;
  }
}
