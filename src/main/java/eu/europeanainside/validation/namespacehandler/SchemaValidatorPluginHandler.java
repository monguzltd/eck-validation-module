package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class SchemaValidatorPluginHandler extends NamespaceHandlerSupport {

  @Override
  public void init() {
    registerBeanDefinitionParser("define", new SchemaValidatorPluginDefinitionParser());
  }

}
