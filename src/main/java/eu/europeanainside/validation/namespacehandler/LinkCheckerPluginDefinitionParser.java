package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import eu.europeanainside.validation.plugins.LiveLinkValidator;

public class LinkCheckerPluginDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition linkValidator =
        new RootBeanDefinition(LiveLinkValidator.class);
    parserContext.registerBeanComponent(new BeanComponentDefinition(linkValidator, element.getAttribute("name")));

    return null;
  }

}
