package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import eu.europeanainside.validation.plugins.ResourceLinkValidator;

public class ResourceValidatorPluginDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition resourceValidator =
      new RootBeanDefinition(ResourceLinkValidator.class);
    String resourceXpath = element.getAttribute("resourceXpath");
    resourceValidator.getPropertyValues().add("resourceXpath", resourceXpath);
    parserContext.registerBeanComponent(new BeanComponentDefinition(resourceValidator, element.getAttribute("name")));

    return null;
  }
}
