package eu.europeanainside.validation.namespacehandler;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import eu.europeanainside.validation.plugins.XSDValidatorPlugin;

public class SchemaValidatorPluginDefinitionParser implements BeanDefinitionParser {

  @Override
  public BeanDefinition parse(Element element, ParserContext parserContext) {
    RootBeanDefinition schemaValidator =
        new RootBeanDefinition(XSDValidatorPlugin.class);
    String xsdPath = element.getAttribute("xsdPath");
    schemaValidator.getPropertyValues().add("xsdLocation", xsdPath);
    String preTransformPath = element.getAttribute("preTransformPath");
    schemaValidator.getPropertyValues().add("preTransformPath", preTransformPath);
    parserContext.registerBeanComponent(new BeanComponentDefinition(schemaValidator, element.getAttribute("name")));

    return null;
  }
}
