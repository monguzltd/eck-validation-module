package eu.europeanainside.validation;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import eu.europeanainside.validation.entities.ResultBean;
import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ValidationException;

/**
 * Class responsible for record validation using profiles.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class Validator {

  /**
   * Validates the given record with the selected profile.
   * @param record The record.
   * @param profileName Profile name for identification.
   * @return Result of the validation.
   * @throws ValidationException
   */
  public ResultBean validateWithProfile(String record, ValidatorProfile profile) throws ValidationException {

    Resource resource = new ByteArrayResource(profile.getBody().getBytes());

    GenericXmlApplicationContext context = new GenericXmlApplicationContext();
    context.load(resource);

    ValidatorPluginManager manager = context.getBean(ValidatorPluginManager.class);

    ResultBean result = manager.validateRecord(record);

    context.close();

    return result;
  }
}
