package eu.europeanainside.validation;

/**
 *
 * @author kszucs
 */
public class ValidationConstants {

  public static final String DEFAULT_PROVIDER = "default";
  public static final String DEFAULT_PROFILE = "lido";
}
