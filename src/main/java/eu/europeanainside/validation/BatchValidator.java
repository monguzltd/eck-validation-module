package eu.europeanainside.validation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import eu.europeanainside.validation.entities.ValidatorProfile;
import eu.europeanainside.validation.exception.ResultNotFoundException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.utils.ConstantUtils;
import eu.europeanainside.validation.utils.ResultDirectoryUtils;

/**
 *
 * @author Gábor
 */
public class BatchValidator {

  private BatchValidatedStateCache validatedStateCache;
  private ExecutorService executor = Executors.newFixedThreadPool(10);

  public void setValidatedStateCache( BatchValidatedStateCache validatedStateCache) {
    this.validatedStateCache = validatedStateCache;
  }
  
  public BatchValidator() throws IOException {
	    Path resultDirPath = ResultDirectoryUtils.getBaseResultDirecotyPath();
	    if (!Files.exists(resultDirPath)) {
	      try {
	        Files.createDirectories(resultDirPath);
	      } catch (IOException e) {
	        throw new IOException("Could not create directory for reults: " + e.getCause());
	      }
	    }
  }

  public boolean validate(List<String> records, ValidatorProfile profile, String providerName, String setNumber) {
    Resource resource = new ByteArrayResource(profile.getBody().getBytes());
    GenericXmlApplicationContext context = new GenericXmlApplicationContext();
    context.load(resource);
  
    if(validatedStateCache.doesValidationSetExist(setNumber, providerName)){
    	return false;
    }else{
		String validatingIdentifire = setNumber;
	    validatedStateCache.putIdentifier(validatingIdentifire, providerName);
	    ValidatorPluginManager simpleManager = context.getBean(ValidatorPluginManager.class);
	    BatchValidatorPluginManager manager = new BatchValidatorPluginManager();
	    manager.setPlugins(simpleManager.getPlugins());
	    manager.setRecordList(records);
	    manager.setProviderName(providerName);
	    manager.setValidatedStateCache(validatedStateCache);
	    manager.setValidatingIdentifire(validatingIdentifire);
	    FutureTask<Void> future = new FutureTask<>(manager);
	
	    executor.execute(future);
	    context.close();
	    return true;
    }
  }

  /**
   * Gets the list of validation sets, which are still in progress.
   * @param providerName name of the provider
   * @return list of sets
   */
  public List<String> getOngoingValidations(String providerName){
		return validatedStateCache.getOngoingValidations(providerName);
	  }
  
  /**
   * Gets the validation result if the validation already finished.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as String
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public String getValidationResult(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
	String status = validatedStateCache.getValidateState(setNumber, providerName);
	
	if(status.equals(ConstantUtils.VALIDATION_OK_STATUS)){
		return ResultDirectoryUtils.getResultFile(setNumber, providerName);
	}else if(status.equals(ConstantUtils.VALIDATION_PROCESSING_STATUS)){
		throw new ValidationException("Validation still in progress!");
	}else{
		throw new ResultNotFoundException();
	}
  }
  
  /**
   * Gets the validation result if the validation already finished.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as Zip
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public byte[] getValidationResultAsZip(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
		String status = validatedStateCache.getValidateState(setNumber, providerName);

		if(status.equals(ConstantUtils.VALIDATION_OK_STATUS)){			
			  return ResultDirectoryUtils.getResultAsZip(setNumber, providerName);
		}else if(status.equals(ConstantUtils.VALIDATION_PROCESSING_STATUS)){
			throw new ValidationException("Validation still in progress!");
		}else{
			throw new ResultNotFoundException();
		}
	  }
  
  /**
   * Gets the validation result if the validation already finished.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file as Zip
   * @throws ResultNotFoundException If the requested result file doesn't exist
   * @throws ValidationException If the validation still in progress
   */
  public String getValidationResultAsJson(String setNumber, String providerName) throws ResultNotFoundException, ValidationException {
		String status = validatedStateCache.getValidateState(setNumber, providerName);

		if(status.equals(ConstantUtils.VALIDATION_OK_STATUS)){			
			  return ResultDirectoryUtils.getResultAsJson(setNumber, providerName);
		}else if(status.equals(ConstantUtils.VALIDATION_PROCESSING_STATUS)){
			throw new ValidationException("Validation still in progress!");
		}else{
			throw new ResultNotFoundException();
		}
	  }
}
