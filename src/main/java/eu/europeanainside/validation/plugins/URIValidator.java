package eu.europeanainside.validation.plugins;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 *
 * @author Gabor Nemeth
 */
public class URIValidator implements ValidatorPlugin {

  private String tagName;
  private String attributeName;
  private static final String LIDO_TYPE = "URI";
  private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public void setAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException {
    try {
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(new StringReader(record)));
      document.getDocumentElement().normalize();
      NodeList elementsByTagName = document.getElementsByTagName(tagName);
      for (int i = 0; i < elementsByTagName.getLength(); i++) {
        Node nNode = elementsByTagName.item(i);
        if (nNode.getNodeType() == Node.ELEMENT_NODE
          && ((Element) nNode).getAttribute(attributeName).equals(LIDO_TYPE)) {
          String uriString = nNode.getTextContent();
          if (uriString != null && !"".equals(uriString)) {
            new URL(uriString);
          }
        }
      }
    } catch (ParserConfigurationException ex) {
      throw new ValidationException("Can't configure the document builder.", ex);
    } catch (SAXException ex) {
      throw new ValidationException("Not a valid XML format.", ex);
    } catch (MalformedURLException ex) {
      try {
        xmlBuilder.addURIValidationError(record, ex.getMessage());
      } catch (ResultXmlBuildingException e) {
        throw new ValidationException("Error occured while building result xml", ex);
      }
    } catch (IOException ex) {
      throw new ValidationException("Has some IO exception, see the stack...", ex);
    }
  }
}
