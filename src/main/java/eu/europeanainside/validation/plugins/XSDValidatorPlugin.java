package eu.europeanainside.validation.plugins;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import net.sf.saxon.dom.DocumentBuilderImpl;
import net.sf.saxon.s9api.SaxonApiException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eu.europeanainside.validation.cache.SchemaCache;
import eu.europeanainside.validation.errorhandler.XSDErrorHandler;
import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.utils.Transformer;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Responsible for the XSD validation of a given record.
 *
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class XSDValidatorPlugin implements ValidatorPlugin {

  private String xsdLocation;
  private String preTransformPath;
  private static final Logger LOGGER = LoggerFactory.getLogger(XSDValidatorPlugin.class);

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException {
    DocumentBuilder docBuilder = new DocumentBuilderImpl();

    Schema schema;
    try {
      long createNod = System.currentTimeMillis();
      XSDErrorHandler errorHandler = new XSDErrorHandler();

      InputStream input = processInput(record);

      Node doc = null;

      doc = docBuilder.parse(new InputSource(input));

      LOGGER.debug("Created Node doc: " + (System.currentTimeMillis() - createNod));
      long createrecordSource = System.currentTimeMillis();
      Source recordSource = new DOMSource(doc);
      LOGGER.debug("Created create record Source: " + (System.currentTimeMillis() - createrecordSource));
      long createschema = System.currentTimeMillis();
      schema = SchemaCache.getSchema(xsdLocation);
      LOGGER.debug("Created schema: " + (System.currentTimeMillis() - createschema));
      long createvalidator = System.currentTimeMillis();
      Validator validator = schema.newValidator();
      validator.setErrorHandler(errorHandler);
      LOGGER.debug("Created validator: " + (System.currentTimeMillis() - createvalidator));
      long createvalidate = System.currentTimeMillis();
      validator.validate(recordSource);
      LOGGER.debug("Created validate: " + (System.currentTimeMillis() - createvalidate));
      validator.reset();

      if (errorHandler.getExceptions().size() > 0) {
        xmlBuilder.addXSDValidationError(record, errorHandler.getExceptions());
      }
    } catch (SAXException | IOException | ResultXmlBuildingException | TransformerException | SaxonApiException e) {
      throw new ValidationException("Record validation failed!", e);
    }
  }

  /**
   * Performs preprocessing in the input if necessary.
   * @param record record string
   * @return processed input stream
   * @throws IOException if an I/O error occurs
   * @throws TransformerException if an XSLT error occurs
   * @throws SaxonApiException general Saxon error
   */
  private InputStream processInput(String record) throws IOException, TransformerException, SaxonApiException {
    InputStream rawInput = IOUtils.toInputStream(record, "UTF-8");
    InputStream finalInput;
    if (preTransformPath != null && !"".equals(preTransformPath)) {
      ClassPathResource resource = new ClassPathResource(preTransformPath);
      finalInput = new ByteArrayInputStream(Transformer.transform(resource.getInputStream(), rawInput).toByteArray());
    } else {
      finalInput = rawInput;
    }
    return finalInput;
  }

  /**
   * Sets the XSD path.
   *
   * @param xsdLocation The new XSD path.
   */
  public void setXsdLocation(String xsdLocation) {
    this.xsdLocation = xsdLocation;
  }

  /**
   * Setter for the preprocessor XSLT.
   * @param preTransformPath XSLT file path
   */
  public void setPreTransformPath(String preTransformPath) {
    this.preTransformPath = preTransformPath;
  }
}
