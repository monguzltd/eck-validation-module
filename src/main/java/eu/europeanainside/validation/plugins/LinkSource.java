package eu.europeanainside.validation.plugins;

import hu.monguz.linkchecker.LinkSourceElement;

/**
 * Just a private static Link source class for the JaDoX link checker.
 */
class LinkSource implements LinkSourceElement {

	private final String url;

	/**
	 * Constructor.
	 * @param url the source link
	 */
	public LinkSource(String url) {
		this.url = url;
	}

	@Override
	public String getUrl() {
		return url;
	}
}