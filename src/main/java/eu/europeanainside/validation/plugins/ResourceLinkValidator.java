package eu.europeanainside.validation.plugins;

import hu.monguz.linkchecker.LinkSourceElement;
import hu.monguz.linkchecker.SimpleLinkSource;

import java.util.Map;

import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathCompiler;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.xml.transform.StringSource;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Validator plugin responsible for checking the existence of digital object links.
 * @author Agoston Berger <aberger@monguz.hu>
 */
public class ResourceLinkValidator implements ValidatorPlugin {

  private static final Logger LOGGER = LoggerFactory.getLogger(ResourceLinkValidator.class);
  private static final int HTTP_OK = 200;
  private String resourceXpath = "/descendant-or-self::lido/administrativeMetadata/resourceWrap/resourceSet/resourceRepresentation/linkResource";

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException {
    try {
      Processor processor = new Processor(false);

      DocumentBuilder builder = processor.newDocumentBuilder();
      XdmNode document = builder.build(new StringSource(record));

      XPathCompiler compiler = processor.newXPathCompiler();
      XPathSelector selector = compiler.compile(resourceXpath).load();
      selector.setContextItem(document);
      boolean empty = true;
      SimpleLinkSource sls = new SimpleLinkSource();
      for (XdmItem value : selector.evaluate()) {
        LOGGER.debug("Found value for xpath: {} with type {}", value.getStringValue(), value.getClass().getName());
        if (value instanceof XdmNode) {
          XdmNode node = (XdmNode) value;
          empty = false;
          sls.addLinkSourceElement(new LinkSource(node.getStringValue()));
        }
      }

      if (empty) {
        xmlBuilder.addResourceValidationError(record, "Resource link missing!");
      } else {
        sls.setResponseCodes();
        Map<LinkSourceElement, Integer> responseCodes = sls.getResponseCodes();
        for (Map.Entry<LinkSourceElement, Integer> entry : responseCodes.entrySet()) {
          Integer httpReply = entry.getValue();
          if (httpReply != HTTP_OK) {
            xmlBuilder.addResourceValidationError(record, "Broken resource link: " + entry.getKey().getUrl());
            LOGGER.info("HTTP request for " + entry.getKey().getUrl() + " resulted in: " + httpReply);
          } else {
            LOGGER.debug("HTTP request for " + entry.getKey().getUrl() + " resulted in: " + httpReply);
          }
        }
      }
    } catch (ResultXmlBuildingException e) {
      LOGGER.error("Exception while building result", e);
      throw new ValidationException(e);
    } catch (SaxonApiException e) {
      LOGGER.error("Exception while evalutating xpath", e);
      throw new ValidationException(e);
    }

  }

  /**
   * @param resourceXpath the resourceXpath to set
   */
  public void setResourceXpath(String resourceXpath) {
    if (resourceXpath == null || "".equals(resourceXpath)) {
      LOGGER.warn("Provided xpath expression is empty, ignoring.");
    } else {
      this.resourceXpath = resourceXpath;
    }
  }
}
