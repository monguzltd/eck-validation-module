package eu.europeanainside.validation.plugins;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import net.sf.saxon.s9api.SaxonApiException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.utils.Transformer;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Validator plugin using Schematron (ISO/IEC FDIS 19757-3) as validating rule set.
 * @author Katalin Szucs <kszucs@monguz.hu>
 *
 */
public class SchematronValidatorPlugin implements ValidatorPlugin {

  private static final String SVRL_TEXT = "svrl:text";
  private static final String SVRL_FAILED_ASSERT = "svrl:failed-assert";
  private String rulesetLocation;
  private String preTransformPath;
  private static final Logger LOGGER = LoggerFactory.getLogger(SchematronValidatorPlugin.class);

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException {
    LOGGER.info("Starting Schematron based validation.");
    try {
      ClassPathResource resource = new ClassPathResource(rulesetLocation);
      LOGGER.info("Record: " + record);
      ByteArrayOutputStream output;
      try (ByteArrayInputStream input = new ByteArrayInputStream(record.getBytes()); InputStream transformerResource = resource.getInputStream()) {
        InputStream finalInput;
        if (preTransformPath != null && !"".equals(preTransformPath)) {
          finalInput = new ByteArrayInputStream(performPreTransform(input).toByteArray());
        } else {
          finalInput = input;
        }
        output = Transformer.transform(transformerResource, finalInput);
      }
      LOGGER.info("Result of transformation: " + output.toString("UTF-8"));

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = factory.newDocumentBuilder();
      Document doc = docBuilder.parse(new ByteArrayInputStream(output.toByteArray()));
      processDocument(record, xmlBuilder, doc);
    } catch (Exception e) {
      throw new ValidationException("Record validation failed!", e);
    }
  }

  /**
   * Process document for failed Schematron asserts.
   * @param record input document
   * @param xmlBuilder result handler instance
   * @param doc parsed and processed document
   * @throws ResultXmlBuildingException if an error occurs during result processing
   */
  private void processDocument(String record, ResultXmlBuilder xmlBuilder, Document doc) throws ResultXmlBuildingException {
    NodeList nodes = doc.getChildNodes();
    NodeList childNodes = nodes.item(0).getChildNodes();
    LOGGER.info("Start processing result.");
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node node = childNodes.item(i);
      String nodeName = node.getNodeName();
      LOGGER.info("Node is: {}", nodeName);
      if (SVRL_FAILED_ASSERT.equals(nodeName)) {
        LOGGER.info("Failed assert found.");
        NodeList failedText = node.getChildNodes();
        for (int j = 0; j < failedText.getLength(); j++) {
          Node textNode = failedText.item(j);
          if (SVRL_TEXT.equals(textNode.getNodeName())) {
            LOGGER.info("Error message found.");
            NodeList textChildNodes = textNode.getChildNodes();
            for (int k = 0; k < textChildNodes.getLength(); k++) {
              Node textNodeChild = textChildNodes.item(k);
              xmlBuilder.addSchematronValidationError(record, textNodeChild.getNodeValue());
            }
          }
        }
      }
    }
  }

  /**
   * Performs preprocessing via XSLT.
   * @param input input document as stream
   * @return processed document
   * @throws IOException if an I/O error occurs
   * @throws TransformerException if an XSLT error occurs
   * @throws SaxonApiException general Saxon error
   */
  private ByteArrayOutputStream performPreTransform(InputStream input) throws IOException, TransformerException,
      SaxonApiException {
    ClassPathResource resource = new ClassPathResource(preTransformPath);
    return Transformer.transform(resource.getInputStream(), input);
  }

  /**
   * Setter for schematron rule file path.
   * @param rulesetLocation rule file path
   */
  public void setRulesetLocation(String rulesetLocation) {
    this.rulesetLocation = rulesetLocation;
  }

  /**
   * Setter for preprocessor XSLT path.
   * @param preTransformPath preprocessor XSLT path
   */
  public void setPreTransformPath(String preTransformPath) {
    this.preTransformPath = preTransformPath;
  }
}
