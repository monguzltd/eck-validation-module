package eu.europeanainside.validation.plugins;

import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Interface for validation plugins.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public interface ValidatorPlugin {

  /**
   * Throws an exception if the validation failed.
   * @param record The record to validate.
   * @throws ValidationException If the validation failed.
   */
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException;

}
