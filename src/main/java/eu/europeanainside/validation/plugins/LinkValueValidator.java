package eu.europeanainside.validation.plugins;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author kszucs
 */
public class LinkValueValidator implements ValidatorPlugin {

  private String linkXpath = "//rightsResource/rightsType/conceptID[@type='URL']";
  private List<String> validUrls = new ArrayList<String>();
  private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  private static final Logger LOGGER = LoggerFactory.getLogger(LinkValueValidator.class);

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder) throws ValidationException {
    try {
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(new StringReader(record)));
      XPath xPath = XPathFactory.newInstance().newXPath();
      NodeList nodes = (NodeList) xPath.evaluate(linkXpath, document.getDocumentElement(), XPathConstants.NODESET);
      if (nodes.getLength() != 0) {
        for (int i = 0; i < nodes.getLength(); i++) {
          Node nNode = nodes.item(i);
          if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            String url = nNode.getTextContent();
            if (!validUrls.contains(url)) {
              xmlBuilder.addResourceValidationError(record, "The link value is invalid: " + url);
            }
          }
        }
      }
    } catch (ParserConfigurationException ex) {
      LOGGER.error("Parser error", ex);
      throw new ValidationException(ex);
    } catch (SAXException ex) {
      LOGGER.error("SAX parser error", ex);
      throw new ValidationException(ex);
    } catch (IOException ex) {
      LOGGER.error("I/O error", ex);
      throw new ValidationException(ex);
    } catch (XPathExpressionException ex) {
      LOGGER.error("Invalid XPath expression: " + linkXpath, ex);
      throw new ValidationException(ex);
    } catch (ResultXmlBuildingException ex) {
      LOGGER.error("Result building failed!", ex);
      throw new ValidationException(ex);
    }
  }

  public void setValidUrls(String urlList) {
    List<String> validUrls = new ArrayList<String>();
    String[] urls = urlList.split(",");
    for (String url : urls) {
      validUrls.add(url.trim());
    }
    this.validUrls = validUrls;
  }

  public void setLinkXpath(String linkXpath) {
    this.linkXpath = linkXpath;
  }
}
