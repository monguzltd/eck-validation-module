package eu.europeanainside.validation.plugins;

import hu.monguz.linkchecker.LinkSourceElement;
import hu.monguz.linkchecker.SimpleLinkSource;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Link checker plugin for record validation.
 * @author Gabor Nemeth
 */
public class LiveLinkValidator implements ValidatorPlugin {

  private static final String XML_DOM_TAG_NAME = "lido:conceptID";
  private static final String XML_ATTR_NAME = "lido:type";
  private static final String LIDO_TYPE = "URI";
  private static final int HTTP_OK = 200;
  private static final Logger LOGGER = LoggerFactory.getLogger(LiveLinkValidator.class);
  private final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

  @Override
  public void validate(String record, ResultXmlBuilder xmlBuilder)
    throws ValidationException {
    try {
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(
        new StringReader(record)));
      document.getDocumentElement().normalize();
      NodeList elementsByTagName = document
        .getElementsByTagName(XML_DOM_TAG_NAME);
      SimpleLinkSource sls = new SimpleLinkSource();
      for (int i = 0; i < elementsByTagName.getLength(); i++) {
        Node nNode = elementsByTagName.item(i);
        if (nNode.getNodeType() == Node.ELEMENT_NODE
          && ((Element) nNode).getAttribute(XML_ATTR_NAME).equals(LIDO_TYPE)) {
          sls.addLinkSourceElement(new LinkSource(nNode.getTextContent()));
        }
      }
      sls.setResponseCodes();
      Map<LinkSourceElement, Integer> responseCodes = sls.getResponseCodes();
      for (Map.Entry<LinkSourceElement, Integer> entry : responseCodes
        .entrySet()) {
        Integer httpReply = entry.getValue();
        if (httpReply != HTTP_OK) {
          xmlBuilder.addLinkValidationError(record, "Broken link: "
            + entry.getKey().getUrl());
          LOGGER.info("HTTP request for " + entry.getKey().getUrl()
            + " resulted in: " + httpReply);
        } else {
          LOGGER.debug("HTTP request for " + entry.getKey().getUrl()
            + " resulted in: " + httpReply);
        }
      }
    } catch (ParserConfigurationException ex) {
      throw new ValidationException("Can't configure the document builder.", ex);
    } catch (SAXException ex) {
      throw new ValidationException("Not a valid XML format.", ex);
    } catch (IOException ex) {
      throw new ValidationException(
        "IO exception in the parser. See the stack...", ex);
    } catch (ResultXmlBuildingException ex) {
      throw new ValidationException("Error occured while building result xml",
        ex);

    }
  }
}
