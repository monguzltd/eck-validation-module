package eu.europeanainside.validation.exception;

/**
 * Exception class representing a case where a nonexistent result was referenced.
 * @author mbathori
 */
public class ResultNotFoundException extends Exception{
	private static final long serialVersionUID = 1L;

	  /** Default constructor. */
	  public ResultNotFoundException() {
	    super();
	  }

	  /**
	   * Constructor.
	   * @param message Error message.
	   */
	  public ResultNotFoundException(String message) {
	    super(message);
	  }

	  /**
	   * Constructor.
	   * @param cause Underlying exception.
	   */
	  public ResultNotFoundException(Throwable cause) {
	    super(cause);
	  }

	  /**
	   * Constructor.
	   * @param message Error message.
	   * @param cause Underlying exception.
	   */
	  public ResultNotFoundException(String message, Throwable cause) {
	    super(message, cause);
	  }

	}
