package eu.europeanainside.validation.exception;

/**
 * Exception class handling errors during validation.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class ValidationException extends Exception {
  private static final long serialVersionUID = 1L;

  /** Default constructor. */
  public ValidationException() {
    super();
  }

  /**
   * Constructor.
   * @param message Error message.
   */
  public ValidationException(String message) {
    super(message);
  }

  /**
   * Constructor.
   * @param cause Underlying exception.
   */
  public ValidationException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructor.
   * @param message Error message.
   * @param cause Underlying exception.
   */
  public ValidationException(String message, Throwable cause) {
    super(message, cause);
  }

}
