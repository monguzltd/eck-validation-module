package eu.europeanainside.validation.exception;

/**
 * Exception class representing a case where a nonexistent profile was referenced.
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public class ProfileNotFoundException extends Exception {
  private static final long serialVersionUID = 1L;

  /** Default constructor. */
  public ProfileNotFoundException() {
    super();
  }

  /**
   * Constructor.
   * @param message Error message.
   */
  public ProfileNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   * @param cause Underlying exception.
   */
  public ProfileNotFoundException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructor.
   * @param message Error message.
   * @param cause Underlying exception.
   */
  public ProfileNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

}
