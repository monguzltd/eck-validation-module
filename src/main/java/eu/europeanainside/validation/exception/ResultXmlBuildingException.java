package eu.europeanainside.validation.exception;

public class ResultXmlBuildingException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	  /** Default constructor. */
	  public ResultXmlBuildingException() {
	    super();
	  }

	  /**
	   * Constructor.
	   * @param message Error message.
	   */
	  public ResultXmlBuildingException(String message) {
	    super(message);
	  }

	  /**
	   * Constructor.
	   * @param cause Underlying exception.
	   */
	  public ResultXmlBuildingException(Throwable cause) {
	    super(cause);
	  }

	  /**
	   * Constructor.
	   * @param message Error message.
	   * @param cause Underlying exception.
	   */
	  public ResultXmlBuildingException(String message, Throwable cause) {
	    super(message, cause);
	  }

}
