package eu.europeanainside.validation.exception;

/**
 * Exception class representing a case when a requested elemnt doesn't exist.
 * @author mbathori
 *
 */
public class ElementNotFoundException extends Exception{
	private static final long serialVersionUID = 1L;

	/** Default constructor. */
	public ElementNotFoundException() {
		super();
	}

	/**
	 * Constructor.
	 * @param message Error message.
	 */
	public ElementNotFoundException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause Underlying exception.
	 */
	public ElementNotFoundException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor.
	 * @param message Error message.
	 * @param cause Underlying exception.
	 */
	public ElementNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
