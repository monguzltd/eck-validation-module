package eu.europeanainside.validation.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import eu.europeanainside.validation.exception.ValidationException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Gábor
 */
public final class SchemaCache {

  /**
   * Default constructor.
   */
  private SchemaCache() {
    throw new IllegalAccessError("Util class.");
  }
  private static final SchemaFactory FACTORY = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
  private static final Logger LOGGER = LoggerFactory.getLogger(SchemaCache.class);
  private static final int MAX_SIZE_OF_CACHE = 100;
  //
  private static final LoadingCache<URL, Schema> SCHEMA_CACHE = CacheBuilder.newBuilder()
    .refreshAfterWrite(1, TimeUnit.DAYS)
    .maximumSize(MAX_SIZE_OF_CACHE)
    .build(
    new CacheLoader<URL, Schema>() {
    @Override
    public Schema load(URL url) throws SAXException {
      LOGGER.debug("Create a new schema: " + url.toString());
      return FACTORY.newSchema(url);
    }
  });

  public static Schema getSchema(String url) throws ValidationException, MalformedURLException {
    return getSchema(new URL(url));
  }

  public static Schema getSchema(URL url) throws ValidationException {
    try {
      return SCHEMA_CACHE.get(url);
    } catch (ExecutionException ex) {
      throw new ValidationException(ex);
    }
  }
}
