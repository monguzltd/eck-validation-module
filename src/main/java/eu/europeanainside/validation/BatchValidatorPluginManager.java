package eu.europeanainside.validation;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.plugins.ValidatorPlugin;
import eu.europeanainside.validation.utils.ConstantUtils;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 *
 * @author Gabor Nemeth
 */
public class BatchValidatorPluginManager implements Callable<Void> {

  private List<ValidatorPlugin> plugins;
  private List<String> recordList;
  private BatchValidatedStateCache validatedStateCache;
  private String validatingIdentifire;
  private String providerName;
  private ResultXmlBuilder resultXmlBuilder;
  private String xmlRecordStartTag;
  private String xmlRecordIdTag;

  public void setRecordList(List<String> recordList) {
    this.recordList = recordList;
  }

  public void setPlugins(List<ValidatorPlugin> plugins) {
    this.plugins = plugins;
  }

  public void setValidatedStateCache(BatchValidatedStateCache validatedStateCache) {
    this.validatedStateCache = validatedStateCache;
  }

  public void setValidatingIdentifire(String validatingIdentifire) {
    this.validatingIdentifire = validatingIdentifire;
  }

  public void setProviderName(String providerName) {
    this.providerName = providerName;
  }

  public void setResultXmlBuilder(ResultXmlBuilder resultXmlBuilder) {
    this.resultXmlBuilder = resultXmlBuilder;
  }

  public void setXmlRecordStartTag(String xmlRecordStartTag) {
    this.xmlRecordStartTag = xmlRecordStartTag;
  }

  public Void call() {
    try {
      resultXmlBuilder = new ResultXmlBuilder(providerName, xmlRecordIdTag);

      List<String> records = resultXmlBuilder.splitRecords(recordList, xmlRecordStartTag);

      for (String xmlRecord : records) {
        resultXmlBuilder.addNewRecord(xmlRecord);
        for (ValidatorPlugin plugin : plugins) {
          plugin.validate(xmlRecord, resultXmlBuilder);
        }
      }

      resultXmlBuilder.writeToFile(providerName, validatingIdentifire + ConstantUtils.XML_FILE_SUFFIX);

      validatedStateCache.updateValidatedStatus(validatingIdentifire, ConstantUtils.VALIDATION_OK_STATUS, providerName);
    } catch (ValidationException | ResultXmlBuildingException ex) {
      String validationFailed = "Validation failed.\n Cause: ";
      validatedStateCache.updateValidatedStatus(validatingIdentifire, validationFailed
        + ex.getMessage()
        + Arrays.toString(ex.getStackTrace())
        + Arrays.toString(ex.getCause().getStackTrace()), providerName);
    }
    return null;
  }
}
