package eu.europeanainside.validation;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.validation.utils.ConstantUtils;
import eu.europeanainside.validation.utils.ResultDirectoryUtils;

/**
 * Stores in a Map the ongoing and finished validations status.
 * @author Gábor
 */
public class BatchValidatedStateCache {

  private final Map<String, Map<String, String>> cache;
  private static final Logger LOGGER = LoggerFactory.getLogger(BatchValidatedStateCache.class);

  /**
   * Default constructor that initialize the cache map from the container directories.
   */
  public BatchValidatedStateCache() {
    this.cache = Collections.synchronizedMap(new HashMap<String, Map<String, String>>());

    Path resultDirPath = ResultDirectoryUtils.getBaseResultDirecotyPath();
    File dir = new File(resultDirPath.toString());
    for (File provider : dir.listFiles()) {
      Map<String, String> set = new HashMap<String, String>();

      for (File result : provider.listFiles()) {
        set.put(FilenameUtils.removeExtension(result.getName()), ConstantUtils.VALIDATION_OK_STATUS);
      }
      cache.put(provider.getName(), set);
    }
  }

  /**
   * The manager put the set number into the cache when the validation starts.
   * @param setNumber set's number
   * @param providerName name of the provider
   */
  public void putIdentifier(String setNumber, String providerName) {
    Map<String, String> status = new HashMap<String, String>();
    status.put(setNumber, ConstantUtils.VALIDATION_PROCESSING_STATUS);
    cache.put(providerName, status);
  }

  /**
   * Selects the validation set by the given set number and provider name, and updates the status.
   * @param setNumber set's number
   * @param state new status
   * @param providerName name of the provider
   * @throws IllegalArgumentException If the validation set or the provider doesn't exist
   */
  public void updateValidatedStatus(String setNumber, String state, String providerName) throws IllegalArgumentException {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        status.put(setNumber, state);
      } else {
        LOGGER.error("The provider doesn't contain the following id: " + setNumber);
        throw new IllegalArgumentException("The provider doesn't contain the following id: " + setNumber);
      }
    } else {
      LOGGER.error("The cache doesn't contain the following provider: " + providerName);
      throw new IllegalArgumentException("The cache doesn't contain the following provider: " + providerName);
    }
  }

  /**
   * Gets the validation set's status
   * @param setNumber set's number
   * @param providerName name of the provider
   * @throws IllegalArgumentException If the validation set or the provider doesn't exist
   * @return the state of the validation
   */
  public String getValidateState(String setNumber, String providerName) throws IllegalArgumentException {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        return status.get(setNumber);
      } else {
        LOGGER.error("The provider doesn't contain the following id: " + setNumber);
        throw new IllegalArgumentException("The provider doesn't contain the following id: " + setNumber);
      }
    } else {
      LOGGER.error("The cache doesn't contain the following provider: " + providerName);
      throw new IllegalArgumentException("The cache doesn't contain the following provider: " + providerName);
    }
  }

  /**
   * Gets the list of validation sets, which are still in progress.
   * @param providerName name of the provider
   * @return list of sets
   * @throws IllegalArgumentException If the provider doesn't exist
   */
  public List<String> getOngoingValidations(String providerName) throws IllegalArgumentException {
    List<String> result = new ArrayList<String>();

    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      for (Map.Entry<String, String> entry : status.entrySet()) {
        if (entry.getValue().equals(ConstantUtils.VALIDATION_PROCESSING_STATUS)) {
          result.add(entry.getKey());
        }
      }
    } else {
      LOGGER.error("The cache doesn't contain the following provider: " + providerName);
      throw new IllegalArgumentException("The cache doesn't contain the following provider: " + providerName);
    }
    return result;
  }

  /**
   * Checks that the requested set exists or not.
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return true if the requested set exists, else false
   * @throws IllegalArgumentException If the provider doesn't exist
   */
  public boolean doesValidationSetExist(String setNumber, String providerName) {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
