package eu.europeanainside.validation;

import java.util.ArrayList;
import java.util.List;

import eu.europeanainside.validation.entities.ResultBean;
import eu.europeanainside.validation.exception.ResultXmlBuildingException;
import eu.europeanainside.validation.exception.ValidationException;
import eu.europeanainside.validation.plugins.ValidatorPlugin;
import eu.europeanainside.validation.utils.ResultDirectoryUtils;
import eu.europeanainside.validation.xmlbuilders.ResultXmlBuilder;

/**
 * Manages validator plugins.
 *
 * @author Katalin Szűcs <kszucs@monguz.hu>
 */
public final class ValidatorPluginManager {

  private ResultXmlBuilder resultXmlBuilder;
  private List<ValidatorPlugin> plugins;
  private String xmlRecordStartTag;
  private String xmlRecordIdTag;

  public void setPlugins(List<ValidatorPlugin> plugins) {
    this.plugins = plugins;
  }

  public List<ValidatorPlugin> getPlugins() {
    return plugins;
  }

  public ResultXmlBuilder getErrorXmlBuilder() {
    return resultXmlBuilder;
  }

  public void setXmlRecordStartTag(String xmlRecordStartTag) {
    this.xmlRecordStartTag = xmlRecordStartTag;
  }

  public void setXmlRecordIdTag(String xmlRecordIdTag) {
    this.xmlRecordIdTag = xmlRecordIdTag;
  }

  public ResultBean validateRecord(String record) throws ValidationException {
    ResultBean result = new ResultBean();

    try {
      resultXmlBuilder = new ResultXmlBuilder(xmlRecordIdTag);

      List<String> recordList = new ArrayList<String>();
      recordList.add(record);
      List<String> records = resultXmlBuilder.splitRecords(recordList, xmlRecordStartTag);
      result.setSuccessful(true);

      for (String xmlRecord : records) {
        resultXmlBuilder.addNewRecord(xmlRecord);
        for (ValidatorPlugin plugin : plugins) {
          plugin.validate(xmlRecord, resultXmlBuilder);
        }
        result.setSuccessful(!resultXmlBuilder.hasErrors());
      }
      result.setResultXml(resultXmlBuilder.documentToString());
      result.setResultJson(ResultDirectoryUtils.xmlToJson(result.getResultXml()));

    } catch (ResultXmlBuildingException e) {
      throw new ValidationException(e.getMessage());
    }
    return result;
  }
}
